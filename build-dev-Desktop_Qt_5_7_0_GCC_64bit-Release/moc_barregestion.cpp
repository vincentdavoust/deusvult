/****************************************************************************
** Meta object code from reading C++ file 'barregestion.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../QtProject_DeusVult/barregestion.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'barregestion.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_barregestion_t {
    QByteArrayData data[9];
    char stringdata0[207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_barregestion_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_barregestion_t qt_meta_stringdata_barregestion = {
    {
QT_MOC_LITERAL(0, 0, 12), // "barregestion"
QT_MOC_LITERAL(1, 13, 30), // "on_actionSpecialButton_clicked"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 27), // "on_apocalipseButton_clicked"
QT_MOC_LITERAL(4, 73, 23), // "on_messieButton_clicked"
QT_MOC_LITERAL(5, 97, 27), // "on_apparitionButton_clicked"
QT_MOC_LITERAL(6, 125, 27), // "on_construireButton_clicked"
QT_MOC_LITERAL(7, 153, 26), // "on_conquerirButton_clicked"
QT_MOC_LITERAL(8, 180, 26) // "on_entrainerButton_clicked"

    },
    "barregestion\0on_actionSpecialButton_clicked\0"
    "\0on_apocalipseButton_clicked\0"
    "on_messieButton_clicked\0"
    "on_apparitionButton_clicked\0"
    "on_construireButton_clicked\0"
    "on_conquerirButton_clicked\0"
    "on_entrainerButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_barregestion[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    0,   54,    2, 0x08 /* Private */,
       8,    0,   55,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void barregestion::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        barregestion *_t = static_cast<barregestion *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionSpecialButton_clicked(); break;
        case 1: _t->on_apocalipseButton_clicked(); break;
        case 2: _t->on_messieButton_clicked(); break;
        case 3: _t->on_apparitionButton_clicked(); break;
        case 4: _t->on_construireButton_clicked(); break;
        case 5: _t->on_conquerirButton_clicked(); break;
        case 6: _t->on_entrainerButton_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject barregestion::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_barregestion.data,
      qt_meta_data_barregestion,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *barregestion::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *barregestion::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_barregestion.stringdata0))
        return static_cast<void*>(const_cast< barregestion*>(this));
    return QWidget::qt_metacast(_clname);
}

int barregestion::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
