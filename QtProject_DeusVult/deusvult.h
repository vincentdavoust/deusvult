/*
** DeusVult.h for DeusVult in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sun Nov 13 12:45:48 2016 Vincent Davoust
** Last update Tue Nov 29 12:04:22 2016 Vincent Davoust
*/


#ifndef DEUSVULT_H_
# define DEUSVULT_H_


#define MMAX_PLAYERS 4
#define MMAX_TAILLE 25
#define MMIN_PLAYERS 1
#define MMIN_TAILLE 5
#define TAILLE 4
#define NAME_MAX_LEN 32
#define DEBUG 1
#define BYTES_CASE 12
#define BYTES_JOUEUR (9+NAME_MAX_LEN)
#define IMG_PATH ":/img/img/"
#define BACKGROUND "stars.jpeg"
#define BOARD_BG "stars.jpeg"
#define BARRE_WIDTH 770


#define FORWARD(method) try { method; } catch (NWException &e) {\
    std::cout<<"FORWARDING Exception" << std::endl; \
    char* newError = new char[1024];\
    (new misc_dv())->mcopy(newError, e.toString(), 1024);\
    throw NWException(newError);\
    }



static int MAX_PLAYERS =2;
static int BUFFER_LENGTH =(TAILLE*TAILLE*BYTES_CASE+8+MAX_PLAYERS*(BYTES_JOUEUR));

static int getBUFFER_LENGTH() {return BUFFER_LENGTH;}
static void setBUFFER_LENGTH(int i) {BUFFER_LENGTH = i;}
static int getMAX_PLAYERS() {return MAX_PLAYERS;}
static void setMAX_PLAYERS(int i) {MAX_PLAYERS = i;}




#include "misc_dv.h"


#include <vector>
#include <string>

typedef bool boolean;

enum Building {vide, chapelle, eglise, monastere, cathedrale, ruine};
enum ArcheType {monetaire, spirituel, guerrier, parasite,
               TypeFirst=monetaire, TypeLast=parasite};

// forward declare all classes
class Plateau;
class Joueur;
class Eglise;
class Case;
class Region;

class NWException;
class Client;
class Serveur;
class Reseau;

class Representant;
class TypeGuerrier;
class TypeSpirituel;
class TypeParasite;
class TypeMonetaire;

/*
// include all header files
#include "representant.h"
#include "typeguerrier.h"
#include "typeparasite.h"
#include "typespirituel.h"
#include "typemonetaire.h"

#include "plateau.h"
#include "region.h"
#include "case.h"
#include "eglise.h"
#include "building.h"
#include "joueur.h"

#include "nwexception.h"
#include "reseau.h"
#include "serveur.h"
#include "client.h"

#include "mainwindow.h"
#include "ui_barregestion.h"
#include "barregestion.h"
#include "view.h"
*/



#endif /* !DEUSVULT_H_ */
