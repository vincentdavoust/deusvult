#ifndef CASE_H
#define CASE_H

#include "deusvult.h"
#include <QColor>
#include <QGraphicsItem>
#include "plateau.h"

class Case : public QGraphicsItem
{
public:
    Case(const QColor &color, int x, int y,Plateau *monPlateau);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) ;

    /**  void creeEglise();
    void getEglise();
    Case getState();
    bool Update(Case uneCase);**/
    boolean isSelected  = false;
    boolean isEnnemie  = false;
    boolean isEglise  = false;
    boolean test = false;
    int isOccuped  = getMAX_PLAYERS();

    int getId();
    void build();
    int fight(int attaquant);
    int getOwner();
    void operator << (const char* j);
    const char* toBytes();
    int getPop();
    void setPop(int pop);

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override ;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override ;

private:
    int x;
    int y;
    Plateau *monPlateau;
    int population;
    //  Eglise eglise;

    QColor color;
    QVector<QPointF> stuff;

};



#endif // CASE_H
