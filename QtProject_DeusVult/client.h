/*
** Client.h for Client in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:45:29 2016 Vincent Davoust
*/

#ifndef CLIENT_H_
# define CLIENT_H_

#include "deusvult.h"

#include "reseau.h"


class Client : public Reseau {


 public :
  /**
   * Constructor, connects to a distant server
   * @param ip : ipv4 address in standard format, as a string
   * @param port : tcp port to connect to, as int
   */
  Client(const char* ip, int port);

  void updateBufferLength(int i);

};

#endif /* !CLIENT_H_ */
