#include "plateau.h"


void Plateau::listenGame() {

// while not end of game
    // listen for turn
    // if end of game
        // exit
    // else if turn
        // play
        // send status
    // else
        // update status


    try {
        char *data = new char[getBUFFER_LENGTH()];
        data[0] = this->typeRepresentant;
        (new misc_dv())->mcopy(data+1, this->nomJoueur, NAME_MAX_LEN);
        ((Client*)this->network)->sendEtat(data);
        std::cout << "Sending representant data" << std::endl;

    } catch (NWException &e) {
        std::cout << "exceptiong - sending representant data" << std::endl;
        std::string error = e.toString();
        QMessageBox::information(0, QString("Network Error"), QString(error.c_str()), QMessageBox::Ok);

    }

    update();
    char* s = new char[getBufferSize()];
    while (this->winner == MMAX_PLAYERS) {
       netGet(s);
       for (int i=0;i<getNbPlayers();i++){
       std::cout << "received player name : "<< joueur[i]->getRepresentant()->nom << std::endl;
       }
       std::cout << s << std::endl;
       std::cout << "Received a new state ! Turn of player " << this->turn << std::endl;
       std::cout << "Winner : " << this->winner << std::endl;
        update();
        if (this->winner != MMAX_PLAYERS) { // some player has won
            if (this->winner == this->playerId)
                win();
            else
                lose();

            return ;

        }
        else if (this->turn == this->playerId) {
            this->isTurn = true;
            std::cout << "My turn ! - player " <<playerId << std::endl;

            this->play();
            std::cout << "Sending what i did !" << std::endl;
            netPut(this->toBytes());
            this->turn++;
        }
   }
    delete s;

}

