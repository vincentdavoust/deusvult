#include "barregestion.h"
#include "ui_barregestion.h"

barregestion::barregestion(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::barregestion)
{
    ui->setupUi(this);
    ui->label->hide();
    ui->progressBar->hide();
}

barregestion::~barregestion()
{
    delete ui;
}


void barregestion::on_actionSpecialButton_clicked()
{
    switch (this->archType) {
    case monetaire :
        this->representant->quette();
        break;
    case spirituel :
        this->representant->evangelisation();
        break;
    case guerrier :
        this->representant->guerre();
        break;
    case parasite :
        Joueur* j = 0;
        // popup de selection de victime

        this->representant->suivre(j);
        break;

    }


    this->plateau->passTurn();
}

void barregestion::on_apocalipseButton_clicked() {
    if (apocalipse_go > 0)
        return;
    apocalipse_go++;

    for (int i=0; i<plateau->getTaille(); i++) {
        for (int j=0; j<plateau->getTaille(); j++) {
            if (plateau->cases[i][j]->getOwner() == plateau->getPlayerId())
                plateau->cases[i][j]->setPop(plateau->cases[i][j]->getPop() / 3);
            else if (plateau->cases[i][j]->getOwner() != plateau->getNbPlayers())
                plateau->cases[i][j]->setPop(plateau->cases[i][j]->getPop() / 4);
        }
    }


    this->plateau->passTurn();
}

void barregestion::on_messieButton_clicked() {
    if (plateau->getSelection().size() != 1 || messie_go > 1)
        return;

    messie_go++;
    // convert and build church on each adjacent tile
    std::list<Case*>::iterator it = plateau->getSelection().begin();
    Case* c = *it;
    c->isOccuped = plateau->getPlayerId();
    c->isEglise = true;

    for (int i=0; i<plateau->getTaille(); i++) {
        for (int j=0; j<plateau->getTaille(); j++) {
            if( plateau->cases[i][j]->getId() == c->getId()){

                if (j>0){
                    plateau->cases[i][j-1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i][j-1]->isEglise = true;
                    plateau->cases[i][j-1]->update();

                }
                if (j<9){
                    plateau->cases[i][j+1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i][j+1]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i>0){
                    plateau->cases[i-1][j]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i-1][j]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i>0){
                    plateau->cases[i+1][j]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i+1][j]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i>0 && j>0){
                    plateau->cases[i-1][j-1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i-1][j-1]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i<9 && j<9){
                    plateau->cases[i+1][j+1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i+1][j+1]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i>0 && j<9){
                    plateau->cases[i-1][j+1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i-1][j+1]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                if (i<9 && j>0){
                    plateau->cases[i+1][j-1]->isOccuped = plateau->getPlayerId();
                    plateau->cases[i+1][j-1]->isEglise = true;
                    plateau->cases[i][j-1]->update();
                }
                plateau->cases[i][j]->update();
                plateau->cases[i][j]->isSelected = false;
            }
        }
    }

    this->plateau->passTurn();
}

void barregestion::on_apparitionButton_clicked() {
    if (plateau->getSelection().size() != 1)
        return;

    // convert and build church on each adjacent tile
    std::list<Case*>::iterator it = plateau->getSelection().begin();
    Case* c = *it;

    for (int i=0; i<plateau->getTaille(); i++) {
        for (int j=0; j<plateau->getTaille(); j++) {
            if( plateau->cases[i][j]->getId() == c->getId()){
                if (plateau->joueur[plateau->getPlayerId()]->getArgent() < TUNES_APPARITION || plateau->cases[i][j]->getOwner() != plateau->getPlayerId())
                    return;
                plateau->joueur[plateau->getPlayerId()]->depenserArgent(TUNES_APPARITION);
                updateNbTroupes(plateau->joueur[plateau->getPlayerId()]->entrainerTroupes(plateau->cases[i][j]->getPop()));
                std::cout << plateau->cases[i][j]->getPop() <<" = nouveau nombre de troupes apres aparition : " << plateau->joueur[plateau->getPlayerId()]->getTroupes() << std::endl;
                plateau->cases[i][j]->isSelected = false;
            }
        }
    }

    this->plateau->passTurn();
}

void barregestion::on_construireButton_clicked() {
    int builds = 0;
    if (plateau->getSelection().size() == 0)
        return;

    std::list<Case*> c = plateau->getSelection();
    for (std::list<Case*>::iterator it = c.begin(); it != c.end(); it++) {
        if (plateau->joueur[plateau->getPlayerId()]->getArgent() >= TUNES_BUILD && ((Case*)(*it))->isOccuped == plateau->getPlayerId()) {
            ((Case*)(*it))->build();
            plateau->joueur[plateau->getPlayerId()]->depenserArgent(TUNES_BUILD);
            builds++;
        }
        ((Case*)(*it))->isSelected = false;
        ((Case*)(*it))->update();

    }


    if (builds != 0) {

        this->plateau->passTurn();
    }
}

void barregestion::on_conquerirButton_clicked() {
    int conquers = 0;
    std::cout << "Conquering "<< plateau->getSelection().size() << " tiles " << std::endl;

    if (plateau->getSelection().size() == 0)
        return;
    std::list<Case*> c = plateau->getSelection();
    for (std::list<Case*>::iterator it = c.begin(); it != c.end(); it++) {
        std::cout << "Conquering : remaining " << plateau->joueur[plateau->getPlayerId()]->getTroupes() << std::endl;
        if (plateau->joueur[plateau->getPlayerId()]->getTroupes() > 0 && ((Case*)(*it))->isOccuped != plateau->getPlayerId()) {
            conquers ++;
            plateau->joueur[plateau->getPlayerId()]->perdreTroupes(((Case*)(*it))->fight(plateau->getPlayerId()));

        }
        ((Case*)(*it))->isSelected = false;

        ((Case*)(*it))->update();
    }
    if (conquers != 0)
        this->plateau->passTurn();
}


void barregestion::on_entrainerButton_clicked() {
    // idéalement, 10% de la population totale des cases controlees en plus comme troupes, max 100%

    unsigned int candidats = ui->nbCroyants->value() - ui->nbTroupes->value();
    std::cout << " candida "<< candidats << std::endl ;


    updateNbTroupes(    plateau->joueur[plateau->getPlayerId()]->entrainerTroupes(candidats / 4));
    this->plateau->passTurn();
}

void barregestion::updateNbCroyants(int nb) {
    ui->nbCroyants->display(nb);



}
void barregestion::updateNbTroupes(int nb) {
    ui->nbTroupes->display(nb);


}
void barregestion::updateEnnemis() {

    std::string name[MMAX_PLAYERS];
    std::string image[MMAX_PLAYERS];
    for (int i=0, j=0; j<plateau->getNbPlayers(); i++, j++) {

        if (j == plateau->getPlayerId()) {
            i--;
            continue;
        }
        image[i] =  IMG_PATH;

        if(plateau->joueur[j]->getRepresentant()->type == 0){
            image[i] += "Guerrier-31x31.jpg";
        }
        else if(plateau->joueur[j]->getRepresentant()->type == 1){
            image[i] += "Monetaire-31x31.jpg";
        }else if(plateau->joueur[j]->getRepresentant()->type == 2){
            image[i] += "Parasite-31x31.jpg";

        }else if(plateau->joueur[j]->getRepresentant()->type == 3){
            image[i] += "Spirituel-31x31.jpg";

        }else {
            image[i] += "representant-31x31.png";
        }
        //image[i] += ");\n";
        std::cout << "Ennemy " << i << " has background " << image[i]<< std::endl;

        name[i] = plateau->joueur[j]->getRepresentant()->nom;
    }

    if (plateau->getNbPlayers() == 1) {
        name[0] = "dummy";
        image[0] = IMG_PATH; image[0] += "representant-31x31.png";
    }

    ui->ennemis->setEnemi_1(name[0], image[0]);
    std::cout << "ennemy 1 written"<<std::endl;

    if (plateau->getNbPlayers() > 2) {
        ui->ennemis->setEnemi_2(name[1], image[1]);
    }
    if (plateau->getNbPlayers() > 3) {
        ui->ennemis->setEnemi_3(name[2], image[2]);
    }
    std::cout << "ennemys written"<<std::endl;
}
void barregestion::updateNbRessources(int nb) {
    ui->nbRessources->display(nb);
}
void barregestion::updateNames() {
    QString *s = new QString(plateau->joueur[plateau->getPlayerId()]->getRepresentant()->getName());
    s->fromLocal8Bit(plateau->joueur[plateau->getPlayerId()]->getRepresentant()->getName(), NAME_MAX_LEN);
    //    /std::cout << s.toStdString().c_str() << std::endl;
    ui->nomRepresentant->setText(*s);
    delete s;
}

void barregestion::updateProgress(int nb) {
    //  ui->progressBar->setValue(nb);
}
void barregestion::updatePicture() {
    std::string name = "background-image : url(";name += IMG_PATH;
    if(plateau->typeRepresentant == 0){
        name += "Guerrier.jpg";

    }
    else if(plateau->typeRepresentant == 1){
        name += "Monetaire.jpg";

    }else if(plateau->typeRepresentant == 2){
        name += "Parasite.jpg";

    }else {
        name += "Spirituel.jpg";
    }
    name += ");\n";
    std::cout << " brush  !!!" << name<< std::endl;



    //QGraphicsScene scene;


/*
    QPixmap slika(name.c_str());
    QPalette paleta;
    paleta.setBrush(this->backgroundRole(), QBrush(slika));
    ui->imageRepresentant->setPalette(paleta);
  */
    ////ui->imageRepresentant->setStyleSheet(name.c_str());
    ui->imageRepresentant->update();

/*
    Avatarpic *avatar = new Avatarpic();
    avatar->setImage(name);
    QGraphicsScene *scene = new QGraphicsScene();
    scene->addItem(avatar);
    std::cout << "avatar added to scene" << std::endl;

    ui->imageRepresentant->setScene(scene);
*/

            //std::string img = "background-image: "; img += IMG_PATH;    img += "deusvult.png"; img += ";";
            //ui->imageRepresentant->setStyleSheet(name.c_str());
    //ui->imageRepresentant->setImage(name);
            //ui->imageRepresentant->setBackgroundRole(QPalette::Background);
            //Brush(QBrush(QImage(name.c_str())));
            //ui->imageRepresentant->setBackgroundBrush(QBrush(QImage(img.c_str())));
    //ui->imageRepresentant->setScene(&scene);
     // QGraphicsPixmapItem image(QPixmap(name.c_str()));
    //scene.addPixmap(QPixmap(name.c_str()));
    ////ui->imageRepresentant->update();
    ui->Representant->update();
    ui->imageRepresentant->setVisible(true);

}
void barregestion::griser(bool g) {
    ui->grisage->setVisible(g);
    ui->grisage_2->setVisible(g);
    ui->grisage_3->setVisible(g);
    update();
}

void barregestion::setRepresentant(Representant *r) {
    //    ui->nomRepresentant = r->getName();
    //    ui->imageRepresentant = r->getImage();
    //ui->archType = r->getType();
    this->representant = r;

}

void barregestion::setJoueur(Joueur *j) {
    this->joueur = j;
}

void barregestion::casesSelected(bool sel) {
    this->ui->apparitionButton->setVisible(sel);
    this->ui->conquerirButton->setVisible(sel);
    this->ui->construireButton->setVisible(sel);
    this->ui->messieButton->setVisible(sel);

    this->ui->apocalipseButton->setVisible(!sel);
    this->ui->actionSpecialButton->setVisible(!sel);
    this->ui->entrainerButton->setVisible(!sel);
}

void barregestion::setPlateau(Plateau *p) {
    this->plateau = p;
    int i = p->getPlayerId();
    std::cout << i << std::endl;
    this->joueur = p->joueur[i];
}

void barregestion::on_giveup_clicked()
{
    std::cout << "abandon" << std::endl;
    plateau->die();
    this->plateau->passTurn();

}
