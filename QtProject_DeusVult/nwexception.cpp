/*
** nwexception.cpp for nwexception in /home/vincent/Documents/ensibs/2a/pooa/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Mon Nov 21 12:04:43 2016 Vincent Davoust
** Last update Mon Nov 21 12:09:49 2016 Vincent Davoust
*/

#include "nwexception.h"

NWException::NWException(const char* e) {
    int len = (1024 > (new misc_dv())->strlen(e)) ? (new misc_dv())->strlen(e) : 1024;
  (new misc_dv())->mcopy(this->msg, e, 1024);
  std::cerr << msg << std::endl;
}

const char* NWException::toString() {
  return this->msg;
}
