#include "avatarpic.h"

Avatarpic::Avatarpic()
{

}

void Avatarpic::setImage(std::__cxx11::string path) {
    image = path;
}

void Avatarpic::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    QImage image(this->image.c_str());

    painter->drawImage(option->rect,image);
}

QRectF Avatarpic::boundingRect()const
{
    return QRectF(0, 0, 31, 31);
}

QPainterPath Avatarpic::shape() const
{
    QPainterPath path;
    path.addRect(14, 14, 82, 42);
    return path;
}

