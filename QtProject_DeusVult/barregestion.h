#ifndef BARREGESTION_H
#define BARREGESTION_H
class barregestion;

#include <QWidget>
#include <QBoxLayout>
#include "avatarpic.h"
#include "deusvult.h"
#include "plateau.h"
#include "representant.h"
#include "joueur.h"
#include "case.h"
#include "ennemis.h"

#define TUNES_BUILD 50
#define TUNES_APPARITION 100


namespace Ui {
class barregestion;
}

class barregestion : public QWidget
{
    Q_OBJECT

public:
    explicit barregestion(QWidget *parent = 0);
    ~barregestion();
    void updateNbCroyants(int nb);
    void updateNbRessources(int nb);
    void updateNbTroupes(int nb);
    void updateProgress(int nb);
    void griser(bool g);
    void setRepresentant(Representant *r);
    void setJoueur(Joueur *j);
    void casesSelected(boolean sel);
    void setPlateau(Plateau* p);
    void updateNames();
    void updatePicture();
    void updateEnnemis();



private slots:
    void on_actionSpecialButton_clicked();
    void on_apocalipseButton_clicked();
    void on_messieButton_clicked();
    void on_apparitionButton_clicked();
    void on_construireButton_clicked();
    void on_conquerirButton_clicked();
    void on_entrainerButton_clicked();

    void on_giveup_clicked();

private:
    Ui::barregestion *ui;

    ArcheType archType;
    Joueur *joueur;
    Representant *representant;
    Plateau *plateau;
    int messie_go = 0;
    int apocalipse_go = 0;

};

#endif // BARREGESTION_H
