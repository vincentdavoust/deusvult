/*
** Reseau.cpp for Reseau in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:02 2016 Vincent Davoust
** Last update Mon Nov 21 12:13:11 2016 Vincent Davoust
*/

#include "reseau.h"
#include "plateau.h"

Reseau::Reseau() {
    id = new char;
    *id = 0;
  socketfd = socket(AF_INET, SOCK_STREAM, 0);
  if (socketfd < 0) {
    throw NWException("ERROR openning socket");
  }

}

Reseau::~Reseau() {
  close(socketfd);
}


void Reseau::sendEtat(const char* data) {
  FORWARD(rawSend(data, plateau->getBufferSize()))
  std::cout << "Gamestate sent" << std::endl;

}

void Reseau::rawSend(const char* data, int size, int newId) {
    newId=((newId==-1)? (*id) : newId);
    char buffer[size+PADDING];
    bzero(buffer, size+PADDING);
    buffer[0] = 42;
    buffer[1] = (char)newId;
    // convert Plateau to byte[]
    (new misc_dv())->mcopy(buffer+2, data, size);

    // write to socket
    std::cout << "Sending "<<size+PADDING<< " bytes of data" << std::endl;


    int check =  write(socketfd, buffer, size+PADDING);

    if (check < 0) {
      throw NWException("ERROR writing to socket. Aborting.");
    }
    std::cout << "data sent" << std::endl;
    (*id)++;

}

void Reseau::getEtat(char* data) {
    FORWARD(rawGet(data, plateau->getBufferSize()))
}

void Reseau::rawGet(char* data, int size, int newId) {

    newId=((newId==-1)? (*id) : newId);
    char *buffer = new char[size+PADDING];
    // read from socket
    std::cout << "Reading "<<size+PADDING<< " bytes of data - expecting id = "<< newId << std::endl;
    int check = read(socketfd, buffer, size+PADDING);
    if (check < 0) {
      throw NWException("ERROR reading from socket. Aborting.");
    }
    std::cout << (int)buffer[0]<< "pkt number "<<(int)buffer[1] << " vs " << (int)newId << std::endl;
    if (buffer[0] == 42 && buffer[1] == newId){
        //std::cout << "bluberflop" << std::endl;
        (*id)++;
        (new misc_dv())->mcopy(data, buffer+2, size);
    }
    else {
        if (buffer[0] != 42)
          throw NWException("ERROR - wrong packet signature, aborting");
        if (buffer[1] != newId)
          throw NWException("ERROR - invalid packet id, aborting");
      throw NWException("Corrupted data transmission, aborting !");
    }

}
