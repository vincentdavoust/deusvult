#include "misc_dv.h"

misc_dv::misc_dv() {

}

int misc_dv::strlen(const char* s) {
    int i = 0;
    while (s[i] != 0)
        i++;
    return i;
}

int misc_dv::mcopy(char* dest, const char* src, int l) {
    for (int i=0; i<l; i++) {
        dest[i] = src[i];
    }
}
