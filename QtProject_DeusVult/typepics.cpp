#include "typepics.h"

TypePics::typePics(QWidget *parent)
    : QGraphicsView(parent) {
    QGraphicsScene *scene = new QGraphicsScene();
    std::string img = IMG_PATH;    img += "representant.png";
    scene->setBackgroundBrush(QBrush(QImage(img.c_str())));
    this->setScene(scene);

    for (ArcheType i=ArcheType::TypeFirst; i<= ArcheType::TypeLast; i++) {
        // add widget woth image to this->typePics

        QGraphicsItem *img = new QGraphicsItem();


        img->setVisible(false);
        images.emplace(i, img);

        scene->addItem(img);
    }
}

void TypePics::choose(ArcheType type) {
    images[selected]->setVisible(false);
    images[type]->setVisible(true);
    selected = type;
}

