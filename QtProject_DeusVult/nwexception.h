/*
** nwexception.h for nwexception in /home/vincent/Documents/ensibs/2a/pooa/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Mon Nov 21 12:07:04 2016 Vincent Davoust
** Last update Mon Nov 21 12:09:28 2016 Vincent Davoust
*/

#ifndef NWEXCEPTION_H_
# define NWEXCEPTION_H_

#include "deusvult.h"
#include <iostream>

class NWException {
private :
    char msg[1024];

public :
    NWException(const char* e);
    const char* toString();
};


#endif /* !NWEXCEPTION_H_ */
