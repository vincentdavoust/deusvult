#ifndef MISC_DV_H
#define MISC_DV_H


class misc_dv
{
public:
    misc_dv();
    int strlen(const char *s);
    int mcopy(char*d, const char*s, int l);
};

#endif // MISC_DV_H
