#ifndef AVATARPIC_H
#define AVATARPIC_H

#include <QGraphicsItem>
#include <QColor>
#include <QtCore>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

class Avatarpic : public QGraphicsItem
{
public:
    Avatarpic();
    void setImage(std::string path);
    void paint(QPainter* painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QRectF boundingRect() const override ;
    QPainterPath shape() const override ;


protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override {}
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override {}
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override {}

signals:

public slots:

private:
    std::string image;
};

#endif // AVATARPIC_H
