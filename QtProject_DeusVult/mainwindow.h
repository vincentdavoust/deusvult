#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWindow>
#include <QThread>
#include <thread>

#include "deusvult.h"
#include "plateau.h"
#include "reseau.h"
#include "serveur.h"
#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addPlateau(Plateau *p);

private slots:
    void on_frame_3_objectNameChanged(const QString &objectName);

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_ok_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_2_clicked();

    void on_frame_3_destroyed();

private:
    Ui::MainWindow *ui;
    Reseau *reseau;
    bool serveur;
    Plateau* plateau;
    QWindow *window;
    char* name;
};

#endif // MAINWINDOW_H
