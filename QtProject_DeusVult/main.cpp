#include "mainwindow.h"
#include "deusvult.h"
#include <QApplication>
//#include "myapp.h"

int main(int argc, char *argv[])
{

    int i = 1;
    QApplication a(argc, argv);
    MainWindow w;
    Plateau *p = new Plateau();
    std::thread game ([=] { p->start(); } );

    w.addPlateau(p);
    w.show();
    a.setStyleSheet("QSplitter {border:none;}");

    try {
        i = a.exec();
    }
    catch (NWException &e) {
        std::cout << "final : " << e.toString() << std::endl;
    }
    return i;

}
