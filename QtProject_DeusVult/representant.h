/*
** Representant.h for Representant in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:12 2016 Vincent Davoust
** Last update Tue Nov 29 12:04:29 2016 Vincent Davoust
*/

#ifndef REPRESENTANT_H_
# define REPRESENTANT_H_



#include "joueur.h"

class Representant {
 private:
  // image
  Joueur* joueur;

  std::vector<Eglise*> eglises;

  int timeBeforeAparition;
  int timeBeforeAppocalypse;
  int timeBeforeMessie;

 public :
  short int type = -1;
  char *nom;
  /**
   * Constructor
   * @param nom : Name of the god
   */
  Representant(const char* nom=(std::string("Ennemi")).c_str(), Joueur *j=0);

  /**
   * Get the name of the representant
   * @return : String name
   */
  const char* getName();

  /**
   * Get the image of the representant
   * @return : QGraphicsView image
   */
//  QGraphicsView getImage();

  /**
   * Ajoute une nouvelle eglise au representant
   * @param e : Eglise a rajouter
   */
  void addEglise(Eglise *e);

  /**
   * Get une eglise par sa case
   * @param c : Case ou est l'eglise
   * @return : Eglise
   */
  Eglise getEglise(Case c);

  /**
   * make an apparition to convert the population of a Case
   * @param c : Case on which to send the messie
   */
  void apparition(Case c);

  /**
   * Make an apocalipse to destroy non beleivers
   */
  void apocalipse();

  /**
   * Send a messie to convert the population of a Case
   * @param c : Case on which to send the messie
   */
  void messie(Case c);
  /**
   * edit the name
   * @brief setName
   * @param n
   */
  void setName(const char* n);


  ArcheType getType();


  void evangelisation() {}
  void quette() {}
  void guerre() {}
  void suivre(Joueur *j) {}
};

#include "deusvult.h"


#endif /* !REPRESENTANT_H_ */
