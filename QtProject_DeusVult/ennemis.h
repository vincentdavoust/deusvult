#ifndef ENNEMIS_H
#define ENNEMIS_H

#include <QWidget>

#include "deusvult.h"
#include "avatarpic.h"

namespace Ui {
class Ennemis;
}

class Ennemis : public QWidget
{
    Q_OBJECT

public:
    explicit Ennemis(QWidget *parent = 0);
    ~Ennemis();

    void setEnemi_1(std::string name, std::string image);
    void setEnemi_2(std::string name, std::string image);
    void setEnemi_3(std::string name, std::string image);

    Ui::Ennemis *ui;
};

#endif // ENNEMIS_H
