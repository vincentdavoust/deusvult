#ifdef MYAPP_H
#define MYAPP_H

#include <QApplication>
#include "nwexception.h"

class MyApp : public QApplication
{
public:
    MyApp(int &i, char**c, int j= ApplicationFlags);
    bool notify(QObject *o, QEvent *e);
};

#endif // MYAPP_H
