#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    setStyleSheet("background:black;");

    std::string bgimg = IMG_PATH;
    bgimg += "deusvult.png";
    QPixmap slika(bgimg.c_str());
    QPalette paleta;
    paleta.setBrush(this->backgroundRole(), QBrush(slika));
    //this->setPalette(paleta);
this->resize(370, 270);
    QRect rect = this->geometry();

    rect.setX(1);
    rect.setY(1);

    ui->frame->setGeometry(rect);
    ui->frame_2->setGeometry(rect);
    ui->frame_3->setGeometry(rect);


    ui->frame_2->hide();
    ui->frame_3->hide();
    ui->wait_co->hide();

    this->serveur = false;

    name = new char[NAME_MAX_LEN];

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_4_clicked()
{
    // create a game server
    // lineEdit : number of tiles
    // lineEdit_2 : port number


    int joueurs = atoi(ui->joueurs->text().toStdString().c_str());
    int taille = atoi(ui->taille->text().toStdString().c_str());
    int port = atoi(ui->port->text().toStdString().c_str());


    joueurs = (joueurs > MMAX_PLAYERS) ? MMAX_PLAYERS : joueurs;
    joueurs = (joueurs < MMIN_PLAYERS) ? MMIN_PLAYERS : joueurs;


    try {
        this->reseau = new Serveur(port, joueurs);
    } catch (NWException &e) {
        QMessageBox::information(0, QString("Error"), QString(e.toString()), QMessageBox::Ok);
        return;
    }


    std::cout << "Listening on port " << ui->port->text().toStdString()<< std::endl;
    this->ui->wait_co->show();
    this->update();

    this->serveur = true;

    ui->frame_3->close();
    this->close();

    // show game

    plateau->setServer();
    plateau->setNetwork(this->reseau);
    plateau->setType(ui->comboBox->currentIndex());
    plateau->updateNbPlayers(joueurs);
    plateau->updateSize(taille);
   memcpy(name,ui->lineEdit->text().toStdString().c_str(),(int)ui->lineEdit->size().height());

   plateau->setPlayerName(name);
   plateau->show();
}


void MainWindow::on_ok_clicked()
{
    // connect to server
    // lineEdit_3 := ip
    // lineEdit_4 := port
    std::cout << "Connecting to "<< ui->lineEdit_3->text().toStdString()<< ":" << ui->port_2->text().toStdString() << std::endl;

    int port = atoi(ui->port_2->text().toStdString().c_str());
    memcpy(name,ui->lineEdit_2->text().toStdString().c_str(),(int)ui->lineEdit_2->size().width());
    const char* ip = ui->lineEdit_3->text().toStdString().c_str();
    try {
        this->reseau = new Client((char*)ip, port);
    } catch (NWException &e) {
        QMessageBox::information(0, QString("Error"), QString(e.toString()), QMessageBox::Ok);
        return;
    }

    ui->frame_2->close();
    this->close();

    // show game
    plateau->setClient();
    plateau->setNetwork(this->reseau);
    plateau->setType(ui->comboBox_2->currentIndex());

    char* hello = new char[8];
    reseau->rawGet(hello, 8, 67);
    plateau->updateNbPlayers(hello[0]<<24 | hello[1]<<16 | hello[2]<<8 | hello[3]);
    plateau->updateSize(hello[4]<<24 | hello[5]<<16 | hello[6]<<8 | hello[7]);
     delete hello;


    plateau->setPlayerName(name);
    plateau->show();

}


void MainWindow::on_pushButton_clicked()
{
    ui->frame->hide();
    ui->frame_3->show();
    //ui->label->hide();
    //ui->lineEdit->hide();

    ui->wait_co->hide();


}

void MainWindow::on_pushButton_3_clicked()
{
    ui->frame->hide();
    ui->frame_2->show();
    ui->wait_co->hide();


}

void MainWindow::on_pushButton_5_clicked()
{
    ui->frame_2->hide();
    ui->frame->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->frame_3->hide();
    ui->frame->show();
}

void MainWindow::on_frame_3_objectNameChanged(const QString &objectName)
{

}
void MainWindow::on_frame_3_destroyed()
{

}

void MainWindow::addPlateau(Plateau *p) {

    this->plateau = p;
}
