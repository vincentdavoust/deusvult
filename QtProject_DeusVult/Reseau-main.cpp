#include "deusvult.h"
#include "serveur.h"
#include "client.h"



int main(int argc, char *argv[]) {
    int port = 1234;
    char ip[16] = "000.000.000.000";

    if (argc == 2) {
	port = atoi(argv[1]);
	Serveur *r = new Serveur(port);
	Plateau *p = new Plateau("hello");
	r->sendEtat(*p);
	char trucmuche[256];
	memcpy(trucmuche, r->getTour(0).toBytes(), BUFFER_LENGTH);
	
	std::cout << trucmuche << "\n";
	delete r;
	delete p;
    } else if (argc == 3) {
	port = atoi(argv[2]);
	memcpy(ip, argv[1], 16);
	Client *r = new Client(ip, port);
	std::cout << r->getEtat().toBytes() << "\n";
	Plateau *p = new Plateau("ca marche");
	r->sendEtat(*p);
	delete r;
	delete p;
    }


}
