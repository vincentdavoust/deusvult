#include "case.h"
#include <QtWidgets>
#include "iostream"
Case::Case(const QColor &color, int x, int y, Plateau *monPlateau)
{

    this->monPlateau = monPlateau;
    this->x = x;
    this->y = y;
    this->color = color;
    this->isOccuped = MMAX_PLAYERS+1;
    this->population = rand() % 100 + 1;
    setZValue((x + y) % 2);

    setFlags(ItemIsSelectable );
    setAcceptHoverEvents(true);
}

QRectF Case::boundingRect()const
{
    return QRectF(0, 0, 110, 70);
}

QPainterPath Case::shape() const
{
    QPainterPath path;
    path.addRect(14, 14, 82, 42);
    return path;
}
void Case::paint(QPainter* painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {

    std::string name = IMG_PATH;
    if(isSelected && monPlateau->getPlayerId()==getOwner() && !isEglise){
        name += "Case-build.png";
        test = true;
        isSelected = true;

    }else if(isSelected && monPlateau->getPlayerId()!=getOwner()){
        name += "Case-conquer.png";
        test = true;
        isSelected = true;
    }else if (isEglise && monPlateau->getPlayerId()==getOwner()){
        name += "Case-eglise.png";
        test = false;


    }else if(isOccuped != MMAX_PLAYERS+1 && isOccuped != monPlateau->getPlayerId()){
        if (isOccuped == 0 || (monPlateau->getPlayerId() == 0 && isOccuped == 1)) {
            name += "Case-rouge.png";
        } else if (isOccuped == 1 || (monPlateau->getPlayerId() < 2 && isOccuped == 2)) {
            name += "Case-bleu.png";
        } else {
            name += "Case-vert.png";
        }
        test = false;


    }else if(isOccuped == monPlateau->getPlayerId()){
        name += "Case-owned.png";
        test = false;


    }
    else {
        name += "Case-vide.png";
        test = false;

    }


    QImage image( name.c_str());

    painter->drawImage(option->rect,image);
}
void Case::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (monPlateau->isTurn == false) {
        isSelected = false;
        update();
        return;
    }
    for (int i=0; i<monPlateau->getTaille(); i++) {
        for (int j=0; j<monPlateau->getTaille(); j++) {
            if( monPlateau->cases[i][j]->getId() == this->getId()){
                std::cout << "bonne cas "<<i << " " << j<< "- player " << isOccuped << " plateau size " << monPlateau->getTaille() << std::endl;
                if(i > 0 && j>0){
                if((monPlateau->cases[i-1][j]->isEglise && monPlateau->cases[i-1][j]->isOccuped == monPlateau->getPlayerId()) || (monPlateau->cases[i][j-1]->isEglise  && monPlateau->cases[i][j-1]->isOccuped == monPlateau->getPlayerId()) ||
                        (i < monPlateau->getTaille()-1 &&(monPlateau->cases[i+1][j]->isEglise && monPlateau->cases[i+1][j]->isOccuped == monPlateau->getPlayerId())) || (j < monPlateau->getTaille()-1 && (monPlateau->cases[i][j+1]->isEglise && monPlateau->cases[i][j+1]->isOccuped == monPlateau->getPlayerId())) ){
                             isSelected = !isSelected;


                    update();


                }
                else {
                    isSelected = false;
                }
                }else if(i == 0 && j > 0 ){
                    if((monPlateau->cases[i][j-1]->isEglise && monPlateau->cases[i][j-1]->isOccuped == monPlateau->getPlayerId()) ||
                            (i < monPlateau->getTaille()-1 &&(monPlateau->cases[i+1][j]->isEglise && monPlateau->cases[i+1][j]->isOccuped == monPlateau->getPlayerId())) || (j < monPlateau->getTaille()-1 && (monPlateau->cases[i][j+1]->isEglise && monPlateau->cases[i][j+1]->isOccuped == monPlateau->getPlayerId())) ){
                       isSelected = !isSelected;
                        update();
                    }
                    else {
                        isSelected = false;
                    }
                }else if(j == 0 && i > 0 ){
                    if((monPlateau->cases[i-1][j]->isEglise && monPlateau->cases[i-1][j]->isOccuped == monPlateau->getPlayerId()) ||
                            (i < monPlateau->getTaille()-1 &&(monPlateau->cases[i+1][j]->isEglise && monPlateau->cases[i+1][j]->isOccuped == monPlateau->getPlayerId())) || (j < monPlateau->getTaille()-1 && (monPlateau->cases[i][j+1]->isEglise && monPlateau->cases[i][j+1]->isOccuped == monPlateau->getPlayerId())) ){
                                 isSelected = !isSelected;
                        update();
                    }
                    else {
                        isSelected = false;
                    }
                }else if(j == 0 && i == 0 ){
                    if((monPlateau->cases[i+1][j]->isEglise && monPlateau->cases[i+1][j]->isOccuped == monPlateau->getPlayerId())
                        || ((monPlateau->cases[i][j+1]->isEglise && monPlateau->cases[i][j+1]->isOccuped == monPlateau->getPlayerId())) ){
                                 isSelected = !isSelected;
                        update();
                    }
                    else {
                        isSelected = false;
                    }
                }


                 break;
            }
        }


    }
}

void Case::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

}

void Case::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

}
void Case::build(){

  isEglise = true;
  update();

}

int Case::fight(int attaquant){

    if(isOccuped != monPlateau->getNbPlayers() ){
        int troupesAtt = 0 ;
        int  troupeDef = 0 ;
        Joueur * def = 0;
        Joueur * Att ;
        for(int i =0 ; i < monPlateau->getNbPlayers() ; i ++){

            if(monPlateau->joueur[i]->getNumero() == attaquant){
                troupesAtt = monPlateau->joueur[i]->getTroupes()/2;
                Att = monPlateau->joueur[i];

            }
            else if(monPlateau->joueur[i]->getNumero() == getOwner()){
                def = monPlateau->joueur[i];
                if(this->isEglise){
                    troupeDef = monPlateau->joueur[i]->getTroupes();
                }
                else {
                    troupeDef = monPlateau->joueur[i]->getTroupes()/4;
                }

            }

        }
        if(troupeDef - troupesAtt < 0 ){ // attaquant gagne
            if (def != 0)
                def->perdreTroupes(troupeDef);
            isOccuped=attaquant;
            Att->perdreTroupes(troupeDef);
            std::cout << "attaquant perd " << troupeDef << " troupes" << std::endl;
            isEglise = false;
            return troupeDef;
        }else{
            if (def != 0)
                def->perdreTroupes(troupesAtt);

            Att->perdreTroupes(troupesAtt);
            std::cout << "attaquant perd " << troupesAtt << " troupes" << std::endl;
            return troupesAtt;
        }

    }
    else {
        isOccuped = attaquant;
        return 0;
    }



}

int Case::getOwner(){
    return isOccuped;

}
int Case::getId(){
    return (x << 16)+y;
}
int Case::getPop(){
    return population;
}
void Case::setPop(int pop){
    this->population = pop;
}

void Case::operator <<(const char* data) {
    this->isEglise =  (bool)data[3];
    this->isOccuped = (data[4] << 24) |
            (data[5] << 16) |
            (data[6] << 8) |
            data[7];
    this->population = (data[8] << 24) |
            (data[9] << 16) |
            (data[10] << 8) |
            data[11];

}

const char* Case::toBytes() {
    char *data = new char[BYTES_CASE];
    data[0] = 0;
    data[1] = 0;
    data[2] = 0;
    data[3] = (char)(isEglise & 0xFF);
    data[4] = (char)(isOccuped>>24 & 0xFF);
    data[5] = (char)(isOccuped>>16 & 0xFF);
    data[6] = (char)(isOccuped>>8 & 0xFF);
    data[7] = (char)(isOccuped & 0xFF);
    data[8] = (char)(population>>24 & 0xFF);
    data[9] = (char)(population>>16 & 0xFF);
    data[10] = (char)(population>>8 & 0xFF);
    data[11] = (char)(population & 0xFF);
    return data;

}

