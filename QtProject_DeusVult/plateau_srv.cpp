#include "plateau.h"

void Plateau::manageGame() {
// while not end of game
    // for each player
        // play turn
        // check if has won
        // upodate all players

    for (int i=0; i<getNbPlayers(); i++)
        connected[i] = true;
    for (int i=getNbPlayers(); i<MMAX_PLAYERS; i++)
        connected[i] = false;

    char hello[8] = {(char)((getNbPlayers()>>24) & 0xFF),
                     (char)((getNbPlayers()>>16) & 0xFF),
                     (char)((getNbPlayers()>>8) & 0xFF),
                     (char)((getNbPlayers()) & 0xFF),
                     (char)((taille>>24) & 0xFF),
                     (char)((taille>>16) & 0xFF),
                     (char)((taille>>8) & 0xFF),
                     (char)(taille& 0xFF)};
std::cout << "Sending number of players : "<< getNbPlayers() << std::endl;
    ((Serveur*)network)->rawSendBc(hello, 8, 67);
// initialiser le jeu

    netGetJoueurs();


    updateBarre();

    char* s = new char[getBufferSize()];
    while (true) {
        // for each connected player
        this->winner = MMAX_PLAYERS;
        if (checkWin()) {
            return;
        }

        for (int i=0; i< getNbPlayers()-1; i++) {
            std::cout << "Player "<<i<<"s turn !" << std::endl;
            if (connected[i] == false)
                continue;

            std::cout << "Winner : " << this->winner << std::endl;
            this->setTurn(i);
            std::cout << "Player "<<i<< " has resources " << joueur[i]->getArgent() << std::endl;

            netPutServer(this->toBytes());
            for (int j=0; j<getNbPlayers();j++)
                std::cout << "sending player name : "<< joueur[j]->getRepresentant()->nom << std::endl;

            std::cout << "Waiting for player "<<i<<"s turn !" << std::endl;


            netGetTurn(s, i);
            std::cout << s << std::endl;
            std::cout << "Got player "<<turn<<"s turn ! He has resources " << joueur[i]->getArgent() << std::endl;

            if (checkWin()) {
                return;
            }

        }

        // update all players
        this->setTurn(getNbPlayers()-1);
        netPutServer(this->toBytes());

        // my turn
        std::cout << "My turn !" << std::endl;
        this->isTurn = true;
        this->play();
        this->isTurn = false;

        this->setTurn(0);
        if (checkWin()) {
            return;
        }
      //  sleep(5);
        // check winner


        // grant money
        for (int k=0; k< getNbPlayers(); k++) {
            if (connected[k] == false)
                continue;
            int croyants = 0;
            for (int i=0; i<taille; i++) {
                for (int j=0; j<taille; j++) {
                    if (cases[i][j]->getOwner() == k)
                        croyants += cases[i][j]->getPop();
                }
            }
            joueur[k]->recolterArgent(croyants);
std::cout << "adding " << croyants << " money to player " << k << " ("<<joueur[k]->getArgent()<< ")"  << std::endl;

        }
        viewBas->updateNbRessources(joueur[playerId]->getArgent());
        updateBarre();


    }
std::cout << "reached end of game " << std::endl;
    delete s;
}

bool Plateau::checkWin() {

    int c[MMAX_PLAYERS+1];
    for (int i=0;i<MMAX_PLAYERS+1;i++)
        c[i] = 0;
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            if (cases[i][j]->getOwner() < MMAX_PLAYERS) {
                c[cases[i][j]->getOwner()] += cases[i][j]->getPop();
                c[MMAX_PLAYERS] += cases[i][j]->getPop();
            }
        }
    }

    for (int i=0;i<MMAX_PLAYERS;i++) {
        if (c[i] == c[MMAX_PLAYERS]) {
            winner = i;
            break;
        }
    }

    if (winner != MMAX_PLAYERS) {
        netPutServer(this->toBytes());
        if (winner==playerId)
            win();
        else
            lose();
        return true;

    }
    return false;
}

void Plateau::netPutServer(const char* s) {
    try {
        ((Serveur*)this->network)->sendEtat(s);
    } catch (NWException &e) {
        std::cout << "exceptiong - putServer" << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(e.toString()), QMessageBox::Ok);
        std::string error = e.toString();
        if (error.find_first_of("Player") != -1) { // a player has disconnected
            disconnectPlayer(error);
        } else {
            while (true){sleep(1);}
            this->exit(-1);
        }
    }
}


void Plateau::netGetServer(char* s) {
    try {
        ((Serveur*)this->network)->getEtat(s);
    } catch (NWException &e) {
        std::cout << "exceptiong - getServer" << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(e.toString()), QMessageBox::Ok);
        return;
        while (true){sleep(1);}
        this->exit(-1);
    }
}
void Plateau::netGetTurn(char* s, int i) {
    try {
        if (connected[i]) {
            ((Serveur*)this->network)->getTour(s, i);
            *this << s;
        }
    } catch (NWException &e) {
        std::cout << "exceptiong - getTurn" << std::endl;
        std::string error = e.toString();
        std::cout << error.c_str() << std::endl << " is there a player ? " <<error.find_first_of("Player") << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(error.c_str()), QMessageBox::Ok);
        if (error.find_first_of("Player") != -1) { // a player has disconnected
            disconnectPlayer(error);
        } else {
            while (true){sleep(1);}
            this->exit(-1);
        }
    }
}

void Plateau::disconnectPlayer(std::string s) {
    for (int i=0; i<MMAX_PLAYERS; i++) {
        if (s.find_first_of(std::to_string(i)) != -1) {
            std::cout << "killing player " << i << std::endl;
            this->die(i);
            connected[i] = false;
            //sleep(1);
           // netPutServer(this->toBytes());
        }
    }
}

void Plateau::netGetJoueurs() {
    try {
        char *data = new char[getBUFFER_LENGTH()];
        for (int i=0; i<getNbPlayers(); i++) {
            if (i == playerId)
                continue;
            if (connected[i]) {
                ((Serveur*)this->network)->getTour(data, i);
                joueur[i]->getRepresentant()->type = data[0];
                (new misc_dv())->mcopy(joueur[i]->getRepresentant()->nom, data+1, NAME_MAX_LEN);
                std::cout << "received player name : "<< data+1 << std::endl;
                std::cout << "received player name : "<< joueur[i]->getRepresentant()->nom << std::endl;
            }
        }
    } catch (NWException &e) {
        std::cout << "exceptiong - getTurn" << std::endl;
        std::string error = e.toString();
        std::cout << error.c_str() << std::endl << " is there a player ? " <<error.find_first_of("Player") << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(error.c_str()), QMessageBox::Ok);
        if (error.find_first_of("Player") != -1) { // a player has disconnected
            disconnectPlayer(error);
        } else {
            while (true){sleep(1);}
            this->exit(-1);
        }
    }
}
