/*
** Client.cpp for Client in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:03 2016 Vincent Davoust
** Last update Mon Nov 21 12:14:06 2016 Vincent Davoust
*/

#include "client.h"


Client::Client (const char* ip, int port) {
  Reseau();
    std::cout << "initialising client" << std::endl;
  // connect to server
  struct sockaddr_in serv_addr;

  // find server
  struct hostent *server = gethostbyname(ip);
  if (server == NULL) {
    throw NWException("No such server found");
  }

  serv_addr.sin_family = AF_INET;
  std::cout << ip << ":" << port << " - " << server->h_addr << std::endl;
  bcopy((char *)server->h_addr,
	(char *)&serv_addr.sin_addr.s_addr,
	server->h_length);
  serv_addr.sin_port = htons(port);
  std::cout << serv_addr.sin_addr.s_addr <<  ":" << serv_addr.sin_port << " - " << serv_addr.sin_zero <<std::endl;
  if (connect(socketfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    throw NWException("ERROR connecting to the server");

  std::cout << "It appears you have succeeded in connecting to game at " << ip << ":" << port << std::endl;






  char buffer[2];
  int check = read(socketfd, buffer, 2);
  if (check < 0)
    throw NWException("ERROR reading from socket");
  if (buffer[0] != 7)
    throw NWException("ERROR - wrong packet signature");
  if (buffer[1] > getMAX_PLAYERS() || buffer[1] < 0)
    throw NWException("ERROR - invalid packet id");


  playerId = 0x00000000 | buffer[1];
  std::cout << "Received " << std::hex << playerId <<std::dec <<  " as player identity" << std::endl;



}

void Client::updateBufferLength(int i) {
    setBUFFER_LENGTH(i);
}
