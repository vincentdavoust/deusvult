/*
** TypeGuerrier.h for TypeGuerrier in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:44:49 2016 Vincent Davoust
*/

#ifndef TYPEGUERRIER_H_
# define TYPEGUERRIER_H_
# define TYPE__

#include "deusvult.h"

#include "representant.h"

class TypeGuerrier : public Representant {
 private:

 public:

  /**
   * Constructor
   * @param nom : Name of the god
   * @param j : associated Joueur
   */

  TypeGuerrier();

  /**
   * Create a war to get a random number of conversions
   * and a random number of dead
   */
  void guerre();

};

#endif /* !TYPEGUERRIER_H_ */
