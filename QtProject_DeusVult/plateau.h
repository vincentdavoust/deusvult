#ifndef PLATEAU_H
#define PLATEAU_H

#include <QWidget>
#include <QMainWindow>
#include <QMessageBox>
#include <list>


#include "deusvult.h"
#include "case.h"
#include "mainwindow.h"
#include "view.h"
#include "barregestion.h"
#include "typespirituel.h"
#include "typeguerrier.h"
#include "typemonetaire.h"
#include "typeparasite.h"


class QGraphicsScene;
class QSplitter;

class Plateau : public QWidget//, public QObject
{
    Q_OBJECT

public slots:
        void start();
public:
    Plateau(QWidget *parent = 0,int taille = TAILLE);
    Plateau(const Plateau &p);
    const char* toBytes();
    void operator << (const char* data); // done




    void setServer();
    void setClient();
    void setNetwork(Reseau *n);
    bool getIsServer();
    Reseau *getNetwork();
    void passTurn();
    int getPlayerId() {return playerId;}
    void setTurn(int i) {turn = i;}
    std::list<Case*> getSelection();
    int getTaille() {return taille;}
    void updateSize(int i);
    void updateNbPlayers(int i);
    int getBufferSize();
    int getNbPlayers();
    int isConnected(int i) {return connected[i];}
    void die();
    void setPlayerName(const char* s);
     void setType(int i);

     int typeRepresentant; // 0 guerrier 1 monetaire 2 parasite default spirituel

    bool isTurn = false;
    Joueur **joueur;
    Case ***cases;


private:
    void setupMatrix();
    void populateScene();
    bool checkWin();
    void exit(int i);
    void netGet(char* data);
    void netGetServer(char* data);
    void netGetTurn(char* data, int i);
    void netPut(const char* s);
    void netPutServer(const char* s);
    void allocate();
    void disconnectPlayer(std::string s);
    void die(int i);
    void netGetJoueurs();

    QGraphicsScene *scene;
    QSplitter *h1Splitter;
    barregestion *viewBas;
    Reseau *network;


    int taille;
    bool isServer;
    bool connected[MMAX_PLAYERS] = {true,true,true,true};


    int winner = MMAX_PLAYERS;
    int playerId;
    int turn;
    int nbJoueurs = MMAX_PLAYERS;
    char* nomJoueur;

    void manageGame();
    void listenGame();

    void play();
    void win();
    void lose();
    void updateBarre();

};


#endif // PLATEAU_H
