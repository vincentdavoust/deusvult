/*
** Joueur.h for Joueur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:46:19 2016 Vincent Davoust
*/

#ifndef JOUEUR_H_
# define JOUEUR_H_
#include <iostream>

#include "deusvult.h"
#include "representant.h"



class Joueur {
 private :
  int argent;
  int numero;
  int troupes;
  Representant *dieu;


 public :
  /**
   * Constructor, create a new player with id number, god, money and troops
   * @param numero id integer of the player
   * @param *dieu pointer to the players Representant
   * @param argent integer, initial money
   * @[param troupes integer, initial troops
   */
  Joueur(int numero,
     Representant *dieu,
     int argent=100,
     int troupes=20);

  /**
   * Get the god of the player, as Representant*
   * @return Representant*
   */
  Representant* getRepresentant();

  /**
   * get the money of the player
   * @return int - money of the player
   */
  int getArgent();

  /**
   * get the current troops of the player
   * @return int - troops of the player
   */
  int getTroupes();

  /**
   * get the id number of the player
   * @return int - id number of the player
   */
  int getNumero();

  /**
   * Spend money
   * @param i amount of money to spend
   * @return int - updated money of the player
   */
  int depenserArgent(int i);

  /**
   * Earn money
   * @param i amount of money to earn
   * @return int - updated money of the player
   */
  int recolterArgent(int i);

  /**
   * Train new troops
   * @param i number of troops to train
   * @return int - updated troops of the player
   */
  int entrainerTroupes(int i);

  /**
   * Lose troops
   * @param i number of troops to lose
   * @return int - updated troops of the player
   */
  int perdreTroupes(int i);

  /**
   * @brief update
   * @param j
   */
  void operator << (const char* data);
  const char* toBytes();
};


#endif /* !JOUEUR_H_ */
