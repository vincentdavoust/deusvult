#ifndef TYPEPICS_H
#define TYPEPICS_H

#include "deusvult.h"
#include <map>
#include <QtCore>
#include <QFrame>
#include <QGraphicsView>
#include <QGraphicsItem>

class TypePics : public QGraphicsView
{
private:
    ArcheType selected;
    std::map<ArcheType, *QGraphicsItem> images;

public:
    typePics(QWidget *parent=0);
    typePics(int h, int w, ArcheType type);
    void chooseType(ArcheType type);

};

#endif // TYPEPICS_H
