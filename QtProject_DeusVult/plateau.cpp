#include "plateau.h"



#include <QHBoxLayout>
#include <QSplitter>
/**
 * @brief Plateau::Plateau
 * @param parent
 * @param taille
 */
Plateau::Plateau(QWidget *parent, int taille)
    : QWidget(parent)
{

    srand (time(NULL));
    scene = new QGraphicsScene(this);
    this->taille = taille;

    QGridLayout *bottom = new QGridLayout;
    QGridLayout *vertical = new QGridLayout;

    viewBas = new barregestion;
    viewBas->setMinimumHeight(175);
    viewBas->setMaximumHeight(175);
    viewBas->setMinimumWidth(BARRE_WIDTH);
    viewBas->setMaximumWidth(BARRE_WIDTH);

    bottom->addWidget(new QWidget(), 0, 0);
    bottom->addWidget(viewBas, 0, 1);
    bottom->addWidget(new QWidget(), 0, 2);

    View *view = new View("Top view ");
    view->view()->setScene(scene);
    QHBoxLayout *layout = new QHBoxLayout;
    vertical->addWidget(view, 0, 0);
    vertical->addLayout(bottom, 1, 0);
    layout->addLayout(vertical);
    setLayout(layout);
    std::string img = IMG_PATH;    img += BOARD_BG;
    scene->setBackgroundBrush(QBrush(QImage(img.c_str())));
    populateScene();

    joueur = new Joueur*[taille];
    for (int i=0; i<taille; i++) {
        joueur[i] = new Joueur(i, new Representant(), 100, 10);
    }

    setWindowTitle(tr("DeusVult - The Game"));
    this->resize(800, BARRE_WIDTH);

    nomJoueur = new char[NAME_MAX_LEN];
    view->resetView();
    std::string bgimg = IMG_PATH;     bgimg += BACKGROUND;
    QPixmap slika(bgimg.c_str());
    QPalette paleta;
    paleta.setBrush(this->backgroundRole(), QBrush(slika));
    setPalette(paleta);
    setStyleSheet("border: none;padding:0px;");
}


void Plateau::populateScene()
{

    this->cases = new Case**[taille];
    for (int i=0; i<taille; i++) {
        this->cases[i] = new Case*[taille];
    }


    // Populate scene
    int xx = 0;
    int nitems = 0;
    for (int i =0, a=0 ;i < (taille*110); i += 110, a++) {
     ++xx;
        int yy = 0;
        for (int j = 0,b=0; j < (taille*70); j += 70,b++) {
            ++yy;
            qreal x = i;
            qreal y = j;
            QColor color(12);
            QGraphicsItem *item = new Case(color, xx, yy, this);

            this->cases[a][b] = (Case*)item;
            item->setPos(QPointF(i, j));
            scene->addItem(item);

            ++nitems;
        }
    }


    this->cases[0][taille/2]->isEglise  =true ;
      this->cases[0][taille/2]->isOccuped  =0;

    this->cases[taille-1][taille/2]->isEglise  =true ;
      this->cases[taille-1][taille/2]->isOccuped  =1;

    if (getNbPlayers() > 2) {
       this->cases[taille/2][taille-1]->isEglise  =true ;
      this->cases[taille/2][taille-1]->isOccuped  =2 ;
    }
    if (getNbPlayers() > 3) {
      this->cases[taille/2][0]->isEglise  =true ;
      this->cases[taille/2][0]->isOccuped  =3;
    }
}

const char* Plateau::toBytes() {
    char *data = new char[getBufferSize()];
    bzero(data, getBufferSize());

    data[0] = (char)(winner>>24 & 0xFF);
    data[1] = (char)(winner>>16 & 0xFF);
    data[2] = (char)(winner>>8 & 0xFF);
    data[3] = (char)(winner & 0xFF);
    data[4] = (char)(turn>>24 & 0xFF);
    data[5] = (char)(turn>>16 & 0xFF);
    data[6] = (char)(turn>>8 & 0xFF);
    data[7] = (char)(turn & 0xFF);

// for each joueur
    // update joueur
    for (int i=0; i<getNbPlayers(); i++) {
        (new misc_dv())->mcopy(data + 8 + i*BYTES_JOUEUR, joueur[i]->toBytes(), BYTES_JOUEUR);
/*        char* s = this->joueur[i]->toBytes();
        for (int k=0; k<BYTES_JOUEUR; k++) {
            data[i*BYTES_JOUEUR + 8 + k] = s[k];
        }
        delete s;*/
    }

// for each case
    // update case
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            //std::cout << i << ";" << j << " - " << this->cases[i][j]<< std::endl;

            const char* s = this->cases[i][j]->toBytes();

            for (int k=0; k<BYTES_CASE; k++) {
                data[(taille*i + j)*BYTES_CASE + 8 + getNbPlayers()*BYTES_JOUEUR + k] = s[k];
            }
            delete s;
        }
    }

    return data;
}
void Plateau::operator <<(const char* data1) {
    char data[getBufferSize()];
    for (int i=0;i<getBUFFER_LENGTH(); i++) {
        data[i] = data1[i];
    }
    const char*chose = data+1000;
    this->winner =  (((unsigned char)data[0]) << 24) |
                    (((unsigned char)data[1]) << 16) |
                    (((unsigned char)data[2]) << 8) |
                     (unsigned char)data[3];
    this->turn =    (((unsigned char)data[4]) << 24) |
                    (((unsigned char)data[5]) << 16) |
                    (((unsigned char)data[6]) << 8) |
                     (unsigned char)data[7];

//    std::cout << (int) data[3]  << ":" << playerId <<" -  player turn byte is " << (int)data[7] << " turn is " << turn << std::endl;

// for each joueur
    // update joueur
    for (int i=0; i<getNbPlayers(); i++) {
        if (i == playerId)
            data[9] = typeRepresentant;

        int k = i*BYTES_JOUEUR;
        *joueur[i] << data + (i*BYTES_JOUEUR)+ 8;
   //     std::cout  << "read troops = " << joueur[i]->getTroupes() << "-<>-" << (int)data[6+ i*BYTES_JOUEUR+ 8] << std::endl;//"---" << (int)data[7+ i*BYTES_JOUEUR+ 8] << std::endl;


    }
// for each case
    // update case
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            //cases[i][j]->update();
//            if ((taille*i + j)*BYTES_CASE + 8 + getNbPlayers()*BYTES_JOUEUR > 1224)
//                std::cout << "rotototototototoototototototototototototototoototototototototot" << std::endl;
            *cases[i][j] << data + (taille*i + j)*BYTES_CASE + 8 + getNbPlayers()*BYTES_JOUEUR ;
        }
    }
    updateBarre();


}

void Plateau::updateBarre() {
    // update screen
    viewBas->updateNbTroupes(joueur[playerId]->getTroupes());
    viewBas->updateNbRessources(joueur[playerId]->getArgent());
    joueur[playerId]->getRepresentant()->setName(nomJoueur);
    viewBas->updateNames();
    viewBas->updateEnnemis();

    int croyants = 0;
    int croyantsTotal = 0;
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            cases[i][j]->update();
            //std::cout << cases[i][j]->getPop() << " - " << i*taille + j << std::endl;
            if (cases[i][j]->getOwner() == playerId)
                croyants += cases[i][j]->getPop();
            croyantsTotal += cases[i][j]->getPop();
        }
    }

    viewBas->updateNbCroyants(croyants);
    viewBas->updateProgress((croyants * 100 ) / croyantsTotal);
    //std::cout << "Population totale : " << croyantsTotal <<  std::endl;

}

Plateau::Plateau(const Plateau &p) {

}

void Plateau::setServer() {
    this->isServer = true;
    setWindowTitle(tr("DeusVult - The Game (Server)"));
}
void Plateau::setClient() {
    this->isServer = false;
}

void Plateau::setNetwork(Reseau *n) {
    this->network = n;
    this->network->addPlateau(this);
    this->playerId = n->getPlayerId();

}

bool Plateau::getIsServer() {
    return this->isServer;
}

Reseau* Plateau::getNetwork() {
    return this->network;
}


void Plateau::start() {

    while (this->isVisible() == false) {}
    viewBas->setPlateau(this);

    updateNbPlayers(getNbPlayers());
    updateSize(taille);
    viewBas->updatePicture();

    if (this->isServer)
        manageGame();
    else
        listenGame();

    std::cout << "Staring one last time at the gamestate..." << std::endl;

}



void Plateau::play() {
    this->isTurn = true;
    viewBas->griser(false);
    std::cout << "degrisé" << std::endl;
// do stuff
    while (isTurn) {
        // listen to clicks on tiles -- establish list
        sleep(1);

    }
// --------
    viewBas->griser(true);

}

void Plateau::passTurn() {
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            //std::cout << i*taille + j << " selected ? " << cases[i][j]->test << std::endl;
            cases[i][j]->isSelected = false;
            cases[i][j]->update();
        }
    }
    this->update();
    this->isTurn = false;
}

void Plateau::win() {
    QMessageBox::information(0, QString("Game Over"), QString("You've Won ! \nCongradulations ! \nYou are the strongest God out there !"), QMessageBox::Ok);
    //this->exit(0);
}

void Plateau::lose() {
    QMessageBox::information(0, QString("Game Over"), QString("You've Lost ! \nBetter luck next time..."), QMessageBox::Ok);
    //this->exit(0);
}

std::list<Case*> Plateau::getSelection() {
    std::list<Case*> sel;
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            //std::cout << "finding if selected - "<<i<<";"<<j<< " - is " << (bool)cases[i][j]->isSelected  << " " << cases[i][j]->isOccuped << std::endl;
            if (cases[i][j]->isSelected) {
                sel.push_front(cases[i][j]);
            }
        }
    }
    return sel;

}

void Plateau::exit(int i) {
    std::cout << "exiting ! " << std::endl;
    while (true){sleep(1);}
   // if (this->network != 0) delete this->network;
}

void Plateau::netGet(char* s) {
    try {
        ((Client*)this->network)->getEtat(s);
        *this << s;
    } catch (NWException &e) {
        std::cout << "exceptiong - get" << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(e.toString()), QMessageBox::Ok);
        while (true) {sleep(1);}
        this->exit(-1);
    }
}



void Plateau::netPut(const char* s) {
    try {
        ((Client*)this->network)->sendEtat(s);
    } catch (NWException &e) {
        std::cout << "exceptiong - put" << std::endl;
        QMessageBox::information(0, QString("Network Error"), QString(e.toString()), QMessageBox::Ok);
        while (true) {sleep(1);}
        this->exit(-1);
    }
}


void Plateau::updateSize(int newTaille) {

    (newTaille > MMAX_TAILLE) ? newTaille = MMAX_TAILLE:newTaille = newTaille;
    (newTaille < MMIN_TAILLE) ? newTaille = MMIN_TAILLE:newTaille = newTaille;

    // realloc cases
    for (int i=0; i<taille; i++) {
        if (this->cases == 0)
            continue;
        for (int j=0; j<taille; j++) {
            scene->removeItem(cases[i][j]);
        }
        delete this->cases[i];
    }
    delete this->cases;
    //std::cout<<"new size : "<< newTaille<<std::endl;



    // change taille
    this->taille = newTaille;
    // update max_buffer for self
    setBUFFER_LENGTH(newTaille*newTaille*BYTES_CASE+8+getNbPlayers()*(BYTES_JOUEUR));
    // update max_buffer for network
    ((Client*)this->network)->updateBufferLength(getBUFFER_LENGTH());


    std::cout<<"Populating scene"<<std::endl;
    populateScene();

    std::cout<<"new size : "<< newTaille<<std::endl;

    update();

}

void Plateau::updateNbPlayers(int newTaille) {
    (newTaille > MMAX_PLAYERS) ? newTaille = MMAX_PLAYERS:newTaille = newTaille;
    (newTaille < MMIN_PLAYERS) ? newTaille = MMIN_PLAYERS:newTaille = newTaille;
    // realloc joueurs
    delete joueur;
    joueur = new Joueur*[newTaille];

    for (int i=0; i<newTaille; i++) {
        if (i == playerId) {
            if(typeRepresentant == 0){
                joueur[i] = new Joueur(i, new TypeGuerrier(), 100, 10);

            }
            else if(typeRepresentant == 1){
                joueur[i] = new Joueur(i, new TypeMonetaire(), 100, 10);

            }else if(typeRepresentant == 2){
                joueur[i] = new Joueur(i, new TypeParasite(), 100, 10);

            }else {
                joueur[i] = new Joueur(i, new TypeSpirituel(), 100, 10);
            }
        }
        else {
            joueur[i] = new Joueur(i, new Representant(), 100, 10);
        }

    }
    // update max_players for self
    setMAX_PLAYERS(newTaille);
    nbJoueurs = newTaille;

    std::cout<<"New player count : "<< newTaille<<std::endl;
}

int Plateau::getBufferSize() {
    std::cout << "buffer length : " << getBUFFER_LENGTH() << std::endl;
    return getBUFFER_LENGTH();
}

int Plateau::getNbPlayers() {
    //std::cout << "players : " << nbJoueurs << std::endl;
    return nbJoueurs;
}

void Plateau::die() {
    this->die(playerId);
}

void Plateau::die(int k){
    for (int i=0; i<taille; i++) {
        for (int j=0; j<taille; j++) {
            if (cases[i][j]->isOccuped == k) {
                //std::cout << "emptying case " << i << ";" << j << std::endl;
                cases[i][j]->isOccuped = MMAX_PLAYERS+1;
                cases[i][j]->isEglise = false;
            }
        }
    }
}

void Plateau::setPlayerName(const char* n) {
    int longueur_nom = (NAME_MAX_LEN > (new misc_dv())->strlen(n))? (new misc_dv())->strlen(n) : NAME_MAX_LEN;
    (new misc_dv())->mcopy(this->nomJoueur, n, longueur_nom);
}
void Plateau::setType(int i) {
this->typeRepresentant = i;
    std::cout << " type :: " << i << "  "<< std::endl ;
}

