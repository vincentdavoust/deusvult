#ifndef MISC
#define MISC
// misc functions
namespace dv {

int strlen(char* s) {
    int i = 0;
    while (s[i] != 0)
        i++;
    return i;
}

int mcopy(char* dest, char* src, int l) {
    for (int i=0; i<l; i++) {
        dest[i] = src[i];
    }
}
}
#endif // MISC

