/*
** Serveur.h for Serveur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:45:45 2016 Vincent Davoust
*/

#ifndef SERVEUR_H_
# define SERVEUR_H_

#include "deusvult.h"

#include "reseau.h"

class Serveur : public Reseau {
 protected :
  int serverSocketfd;
  int *clientSocketfd;


 public :
  /**
   * Set up the game server
   * - need to get somehow the list of players connected
   * @param port : tcp port to listen to, as int
   * @return : true if success, false if failure
   */
  Serveur(int port, int joueurs=2);

  /**
   * Let a player play and get the new game state
   * @param : joueur - optionnal, number of the player to get (default 0)
   * @return : Plateau stipped from all graphic elements
   */
  void getTour(char*data, int joueur = 0);


  /**
   * Send the new state of the game to all client sockets
   * @param plateau : the new Plateau striped from graphical elements
   */
  void sendEtat(const char* plateau);

  void rawSendBc(const char* data, int size, int newId=1);


  /**
   * Destructor - Close the sockets
   */
  ~Serveur();
};




#endif /* !SERVEUR_H_ */
