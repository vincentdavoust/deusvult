/*
** Serveur.cpp for Serveur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:02 2016 Vincent Davoust
** Last update Mon Nov 21 12:11:58 2016 Vincent Davoust
*/

#include "serveur.h"
#include "plateau.h"


Serveur::Serveur(int port, int joueurs) {
  Reseau();
  for (int i=0; i<MMAX_PLAYERS; i++)
      counter[i] = 0;

if (joueurs > 1) {
  clientSocketfd = new int[plateau->getBufferSize()];
  for (int i=0; i<joueurs-1;i++) {
    clientSocketfd[i] = -1;
  }
  struct sockaddr_in serv_addr, cli_addr;
  socklen_t cli_len;

  // bind to port
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);
  if (bind(socketfd, (struct sockaddr *) &serv_addr,
	   sizeof(serv_addr)) < 0)
    throw NWException("ERROR on binding. Is the port free ?");

  // listen
  listen(socketfd, 10);

  // new transmission socket (in socketfd) and backup old socket in serverSocketfd
  serverSocketfd = socketfd;

  for (char i=0; i<joueurs-1; i++) {
     clientSocketfd[i] = accept(serverSocketfd, (struct sockaddr*) &cli_addr, &cli_len);
     if (clientSocketfd[i] < 0) {
        throw NWException("Error to accept connection to client");
     }
  }

  for (char i=0; i<joueurs-1; i++) {
     // send player number to player
     char buffer[2] = {7, i};
     int check = write(clientSocketfd[i], buffer, 2);
     if (check < 0)
       throw NWException("ERROR writing to socket. Aborting");
     std::cout << "Connected to player " << i << std::endl;
  }
  std::cout << "All players seem to have connected to game on port "<< port << std::endl;
  playerId = joueurs-1;
}
    close(serverSocketfd);
}

void Serveur::getTour(char* data, int joueur) {

    if (clientSocketfd[joueur] < 0 || !this->plateau->isConnected(joueur))
      return;
  // configure from which client to get turn
  socketfd = clientSocketfd[joueur];
  id = counter + joueur; // pointer tp counter of that player

  // Reseau::getEtat
  try {
      Reseau::getEtat(data);
  } catch (NWException &e) {
      std::string error = "Player " + std::to_string(joueur) + " has disconnected when it was his turn";

      throw NWException(error.c_str());
  }
}

void Serveur::sendEtat(const char* plateau) {
  bool fails[MMAX_PLAYERS];
  bool raaarg = false;

  for (int i=0; i < this->plateau->getNbPlayers()-1; i++) {
      fails[i] = false;
    if (clientSocketfd[i] < 0 || !this->plateau->isConnected(i))
      continue;
    socketfd = clientSocketfd[i];
    id = counter + i; // poiter to counter of that player
#ifdef DEBUG
    std::cout << "Sending state to client " << i <<"\n";
#endif
    try {
        Reseau::sendEtat(plateau);
    } catch (NWException &e) {
        fails[i] = true;
        raaarg = true;
    }
  }
  std::string errormsg = "Players ";
  for (int i=0; i < this->plateau->getNbPlayers()-1; i++) {
      if (fails[i])
          errormsg += std::to_string(i) + " ";
  }
  errormsg += "has disconnected";

  if (raaarg)
      throw NWException(errormsg.c_str());

}


Serveur::~Serveur() {
#ifdef DEBUG
  std::cout << "\n\ndeleting mothafoka tabernak\n\n";
#endif
//  close(serverSocketfd);
  for (int i=0; i<this->plateau->getNbPlayers()-1;i++) {
    if (clientSocketfd[i] >= 0)
      close(clientSocketfd[i]);
  }
}


void Serveur::rawSendBc(const char* data, int size, int newId) {
    for (int i=0; i < this->plateau->getNbPlayers()-1; i++) {
      if (clientSocketfd[i] < 0 || !this->plateau->isConnected(i))
        continue;
      socketfd = clientSocketfd[i];
      id = counter + i; // pointer
  #ifdef DEBUG
      std::cout << "Sending raw stuff to client " << i <<"\n";
  #endif
      FORWARD(Reseau::rawSend(data, size, newId))
    }

}
