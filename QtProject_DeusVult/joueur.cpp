/*
** Joueur.cpp for Joueur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sun Nov 13 13:10:35 2016 Vincent Davoust
** Last update Mon Nov 21 11:52:40 2016 Vincent Davoust
*/

#include "joueur.h"

Joueur::Joueur(int numero,
               Representant *dieu,
               int argent,
               int troupes) {
  this->numero = numero;
  this->dieu = dieu;
  this->argent = argent;
  this->troupes = troupes;
}


Representant* Joueur::getRepresentant() {
  return this->dieu;
}

int Joueur::getArgent() {
  return this->argent;
}

int Joueur::getTroupes() {
  return this->troupes;
}

int Joueur::getNumero() {
  return this->numero;
}

int Joueur::depenserArgent(int i) {
  this->argent -= i;
  return this->getArgent();
}
int Joueur::recolterArgent(int i) {
  this->argent += i;
  return this->getArgent();
}

int Joueur::entrainerTroupes(int i) {
  this->troupes += i;
  return this->getTroupes();
}

int Joueur::perdreTroupes(int i) {
    std::cout << "Losing "<< i << " troops"<<std::endl;
  this->troupes -= i;
  return this->getTroupes();
}



const char* Joueur::toBytes() {
    char *data = new char[BYTES_JOUEUR];
    data[0] = argent>>24 & 0xFF;
    data[1] = argent>>16 & 0xFF;
    data[2] = argent>>8 & 0xFF;
    data[3] = argent & 0xFF;
    unsigned int t = troupes;
    data[4] = t>>24 & 0xFF;
    data[5] = t>>16 & 0xFF;
    data[6] = t>>8 & 0xFF;
    data[7] = t & 0xFF;


    data[8] = dieu->type;

    for (int i=0; false && i<NAME_MAX_LEN; i++) {
        if (dieu->nom[i] == '\n') {
            dieu->nom[i] = '\0';
            break;
        }
    }
    (new misc_dv())->mcopy(data+9, getRepresentant()->nom, NAME_MAX_LEN);
    std::cout << "toByting player name : "<< getRepresentant()->nom << std::endl;
    std::cout << "toByted player name : "<< data+9 << std::endl;



   // std::cout  << (int)data[4] << (int)data[5] << (int)data[6] <<  "---" <<(int)data[7] << std::endl;
    return data;
}

void Joueur::operator <<(const char* data) {
    this->argent =  (unsigned int) (((unsigned char)data[0]) << 24) |
                    (((unsigned char)data[1]) << 16) |
                    (((unsigned char)data[2]) << 8) |
                     (unsigned char)data[3];
    this->troupes = (unsigned int) ((((unsigned char)data[4]) << 24) |
                    (((unsigned char)data[5]) << 16) |
                    (((unsigned char)data[6])<< 8 ) |
                     (unsigned char)data[7]);
    this->dieu->type = data[8];

    (new misc_dv())->mcopy(dieu->nom, data+9, NAME_MAX_LEN);

    for (int i=0; false && i<NAME_MAX_LEN; i++) {
        if (dieu->nom[i] == '\n') {
            dieu->nom[i] = '\0';
            break;
        }
    }

}
