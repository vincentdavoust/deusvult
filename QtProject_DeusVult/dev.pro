#-------------------------------------------------
#
# Project created by QtCreator 2016-12-31T20:58:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dev
TEMPLATE = app


 CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    barregestion.cpp \
    case.cpp \
    client.cpp \
    joueur.cpp \
    nwexception.cpp \
    plateau.cpp \
    representant.cpp \
    reseau.cpp \
    serveur.cpp \
    typeguerrier.cpp \
    typemonetaire.cpp \
    typeparasite.cpp \
    typespirituel.cpp \
    view.cpp \
    misc_dv.cpp \
    plateau_srv.cpp \
    plateau_cli.cpp \
    myapp.cpp \
    ennemis.cpp \
    avatarpic.cpp \
    typepics.cpp

HEADERS  += mainwindow.h \
    barregestion.h \
    building.h \
    case.h \
    client.h \
    deusvult.h \
    eglise.h \
    joueur.h \
    nwexception.h \
    plateau.h \
    region.h \
    representant.h \
    reseau.h \
    serveur.h \
    typeguerrier.h \
    typemonetaire.h \
    typeparasite.h \
    typespirituel.h \
    view.h \
    misc_dv.h \
    myapp.h \
    ennemis.h \
    avatarpic.h \
    typepics.h

FORMS += \
    barregestion.ui \
    mainwindow.ui \
    ennemis.ui

DISTFILES += \
    ../img/src_files/chaos.jpeg \
    ../img/src_files/sand.jpeg \
    ../img/Guerrier.jpg \
    ../img/Monetaire.jpg \
    ../img/Parasite.jpg \
    ../img/Sprituel.jpg \
    ../img/stars.jpeg \
    ../img/src_files/church.png \
    ../img/src_files/CROIXTEUTONIQUE.png \
    ../img/src_files/hammer.png \
    ../img/src_files/sword.png \
    ../img/Case-bleu.png \
    ../img/Case-bleue-eglise.png \
    ../img/Case-build.png \
    ../img/Case-conquer.png \
    ../img/Case-eglise.png \
    ../img/Case-owned.png \
    ../img/Case-rouge-eglise.png \
    ../img/Case-rouge.png \
    ../img/Case-vert-eglise.png \
    ../img/Case-vert.png \
    ../img/Case-vide.png \
    ../img/Case5.png \
    ../img/deusvult.png \
    ../img/Cases_gimp.xcf

RESOURCES += \
    images.qrc
