/*
** Reseau.h for Reseau in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 12:07:53 2016 Vincent Davoust
*/

#ifndef RESEAU_H_
# define RESEAU_H_

#include <sys/socket.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>


#include "deusvult.h"
#include "nwexception.h"

#define PADDING 100


class Reseau {
 protected :
  int socketfd;
  int playerId;
  char *id;
  Plateau* plateau;
  char counter[MMAX_PLAYERS];

 public :
  int getPlayerId() {return playerId;}

  /**
   * Constructor creating the unbinded socket
   */
  Reseau();

  /**
   * Destrucort, Close the opened socket
   */
  ~Reseau();

  /**
   * Send the new state of the game to the socket
   * @param plateau : the new Plateau striped from graphical elements
   */
  void sendEtat(const char* plateau);

  /**
   * Get the gamestate from the socket
   * @return : Plateau stipped from all graphic elements
   */
  void getEtat(char* data);

  void rawGet(char* data, int size, int id=1);
  void rawSend(const char* data, int size, int id=1);
  void addPlateau(Plateau* p) {plateau=p;}

};


#endif /* !RESEAU_H_ */
