/*
** TypeMonetaire.h for TypeMonetaire in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:44:05 2016 Vincent Davoust
*/

#ifndef TYPEMONETAIRE_H_
# define TYPEMONETAIRE_H_
# define TYPE__

#include "deusvult.h"
#include "representant.h"

class TypeMonetaire : public Representant {
 private:

 public:

  /**
   * Constructor
   * @param nom : Name of the god
   * @param j : associated Joueur
   */
  TypeMonetaire() {
      type = 1;
    Representant();
  }

  /**
   * beg for a random amount of money
   * @return : money collected, as int
   */
  int quette();

};


#endif /* !TYPEMONETAIRE_H_ */
