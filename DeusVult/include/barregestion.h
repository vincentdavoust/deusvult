#ifndef BARREGESTION_H
#define BARREGESTION_H

#include <QWidget>
#include "deusvult.h"

namespace Ui {
class barregestion;
}

class barregestion : public QWidget
{
    Q_OBJECT

public:
    explicit barregestion(QWidget *parent = 0);
    ~barregestion();
    void updateNbCroyants(int nb);
    void updateNbRessources(int nb);
    void updateNbTroupes(int nb);
    void updateProgress(int nb);
    void griser(bool g);
    void setRepresentant(Representant *r);
    void setJoueur(Joueur *j);
    void casesSelected(boolean sel);



private slots:
    void on_actionSpecialButton_clicked();
    void on_apocalipseButton_clicked();
    void on_messieButton_clicked();
    void on_apparitionButton_clicked();
    void on_construireButton_clicked();
    void on_conquerirButton_clicked();
    void on_entrainerButton_clicked();

private:
    Ui::barregestion *ui;

    ArcheType archType;
    Joueur *joueur;
    Representant *representant;
};

#endif // BARREGESTION_H
