/*
** TypeParasite.h for TypeParasite in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:12 2016 Vincent Davoust
** Last update Mon Nov 21 11:44:33 2016 Vincent Davoust
*/

#ifndef TYPEPARASITE_H_
# define TYPEPARASITE_H_

#include "deusvult.h"

#include "representant.h"

class TypeParasite : public Representant {
 private:

 public:

  /**
   * Constructor
   * @param nom : Name of the god
   * @param j : associated Joueur
   */
  TypeParasite(String nom, Joueur *j) {
    Representant(nom, j);
  }

  /**
   * Leach on a representant
   * @param : Representant to leach on
   */
  void suivre(Representant victime);

};


#endif /* !TYPEPARASITE_H_ */
