#ifndef CASE_H
#define CASE_H

#include "eglise.h"
#include <QColor>
#include <QGraphicsItem>

class Case : public QGraphicsItem
{
public:
    Case(const QColor &color, int x, int y);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) ;
  /**  void creeEglise();
    void getEglise();
    Case getState();
    bool Update(Case uneCase);**/

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override ;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override ;

private:
    int x;
    int y;
    int population;
  //  Eglise eglise;

    QColor color;
    QVector<QPointF> stuff;

};



#endif // CASE_H
