/*
** DeusVult.h for DeusVult in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sun Nov 13 12:45:48 2016 Vincent Davoust
** Last update Tue Nov 29 12:04:22 2016 Vincent Davoust
*/

#ifndef DEUSVULT_H_
# define DEUSVULT_H_

#define BUFFER_LENGTH 256
#define MAX_PLAYERS 2
#define NAME_MAX_LEN 32

/*
 * Main include file for project, define stuff here
 */

typedef char* String;
typedef bool boolean;

enum Building {vide, chapelle, eglise, monastere, cathedrale, ruine};
enum ArcheType {monetaire, spirituel, guerrier, parasite};

// forward define all classes
class Plateau;
class Joueur;
class Eglise;
class Case;
class Region;

class NWException;
class Client;
class Serveur;
class Reseau;

class Representant;
class TypeGuerrier;
class TypeSpirituel;
class TypeParasite;
class TypeMonetaire;


#include "barregestion.h"
#include "building.h"
#include "case.h"
#include "client.h"
#include "eglise.h"
#include "joueur.h"
#include "mainwindow.h"
#include "nwexception.h"
#include "plateau.h"
#include "region.h"
#include "representant.h"
#include "reseau.h"
#include "serveur.h"
#include "typeguerrier.h"
#include "typeparasite.h"
#include "typespirituel.h"
#include "typemonetaire.h"
#include "ui_barregestion.h"
#include "view.h"



#endif /* !DEUSVULT_H_ */
