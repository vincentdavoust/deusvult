#ifndef PLATEAU_H
#define PLATEAU_H

#include <QWidget>
#include <QMainWindow>

class QGraphicsScene;
class QSplitter;

class Plateau : public QWidget
{
    Q_OBJECT
public:
    Plateau(QWidget *parent = 0,int taille = 100);

private:
    void setupMatrix();
    void populateScene();

    QGraphicsScene *scene;
    QSplitter *h1Splitter;
   int taille;

};


#endif // PLATEAU_H
