/*
** TypeSpirituel.h for TypeSpirituel in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult/include
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Tue Nov  8 11:15:11 2016 Vincent Davoust
** Last update Mon Nov 21 11:43:53 2016 Vincent Davoust
*/

#ifndef TYPESPIRITUEL_H_
# define TYPESPIRITUEL_H_

#include "deusvult.h"

#include "representant.h"

class TypeSpirituel : public Representant {
 private:

 public:

  /**
   * Constructor
   * @param nom : Name of the god
   * @param j : associated Joueur
   */
  TypeSpirituel(String nom, Joueur *j) {
    Representant(nom, j);
  }

  /**
   * Evangelise to get a random number of conversions
   */
  void evangelisation();

};

#endif /* !TYPESPIRITUEL_H_ */
