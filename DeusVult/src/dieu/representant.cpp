/*
** Representant.cpp for Representant in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sun Nov 13 12:44:35 2016 Vincent Davoust
** Last update Mon Nov 21 11:48:21 2016 Vincent Davoust
*/

#include "representant.h"

Representant::Representant(char* n, Joueur *j) {
  int longueur_nom = (NAME_MAX_LEN > std::strlen(n))? std::strlen(n) : NAME_MAX_LEN;
  std::copy(this->nom, n, longueur_nom);
  this->joueur = j;
}

char* Representant::getName() {
  return nom;
}

void Representant::addEglise(Eglise *e) {
  eglises.push_back(e);
}

Eglise Representant::getEglise(Case c) {
  // how the hell do i do this ???
}

void Representant::apparition(Case c) {

}

void Representant::apocalipse() {

}
