#include "plateau.h"

#include "case.h"
#include "mainwindow.h"
#include "view.h"
#include "barregestion.h"

#include <QHBoxLayout>
#include <QSplitter>

Plateau::Plateau(QWidget *parent, int taille)
    : QWidget(parent)
{
      scene = new QGraphicsScene(this);
    this->taille = taille;
    populateScene();
      QSplitter *vSplitter = new QSplitter;
         vSplitter->setOrientation(Qt::Horizontal);
    h1Splitter = new QSplitter;
    h1Splitter->setOrientation(Qt::Vertical);
vSplitter->addWidget(h1Splitter);


    barregestion *viewBas = new barregestion;
    viewBas;

    View *view = new View("Top view ");
    view->view()->setScene(scene);
    h1Splitter->addWidget(view);
        h1Splitter->addWidget(viewBas);
    QHBoxLayout *layout = new QHBoxLayout;
     layout->addWidget(h1Splitter);
    setLayout(layout);




    setWindowTitle(tr("DeusVult"));
}

void Plateau::populateScene()
{


    //QImage image("/home/desmondes/Documents/POAENSIBS/Graphics/Case.png");

    // Populate scene
    int xx = 0;
    int nitems = 0;
    for (int i =0 ;i < (taille*110)/2; i += 110) {
     ++xx;
        int yy = 0;
        for (int j = 0; j < (taille*70)/2; j += 70) {
            ++yy;
            qreal x = i;
            qreal y = j;
            QColor color(12);
            QGraphicsItem *item = new Case(color, xx, yy);
            item->setPos(QPointF(i, j));
            scene->addItem(item);

            ++nitems;
        }
    }
}
