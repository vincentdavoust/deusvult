#include "barregestion.h"
#include "ui_barregestion.h"

barregestion::barregestion(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::barregestion)
{
    ui->setupUi(this);
}

barregestion::~barregestion()
{
    delete ui;
}


void barregestion::on_actionSpecialButton_clicked()
{
  switch (this->archeType) {
  case monetaire :
    this->representant->quette();
    break;
  case spirituel :
    this->representant->evangelisation();
    break;
  case guerrier :
    this->representant->guerre();
    break;
  case parasite :
    Joueur* j = null;
    // popup de selection de victime

    this->representant->suivre(j);
    break;
  }
}

void barregestion::on_apocalipseButton_clicked() {
    this->updateNbCroyants(14);
    this->updateProgress(74);
}

void barregestion::on_messieButton_clicked() {

}

void barregestion::on_apparitionButton_clicked() {
  // select case

}

void barregestion::on_construireButton_clicked() {

}

void barregestion::on_conquerirButton_clicked() {

}


void barregestion::on_entrainerButton_clicked() {

}

void barregestion::updateNbCroyants(int nb) {
    ui->nbCroyants->display(nb);


}
void barregestion::updateNbTroupes(int nb) {
    ui->nbTroupes->display(nb);


}
void barregestion::updateNbRessources(int nb) {
    ui->nbRessources->display(nb);
}

void barregestion::updateProgress(int nb) {
    ui->progressBar->setValue(nb);
}

void barregestion::griser(bool g) {
    ui->grisage->setVisible(g);
}

void barregestion::setRepresentant(Representant *r) {
    ui->nomRepresentant = r->getName();
    ui->imageRepresentant = r->getImage();
    ui->archType = r->getType();
    this->representant = r;

}

void barregestion::setJoueur(Joueur *j) {
    this->joueur = j;
}

void barregestion::casesSelected(boolean sel) {
  this->ui->apparitionButton->setVisible(sel);
  this->ui->conquerirButton->setVisible(sel);
  this->ui->construireButton->setVisible(sel);
  this->ui->messieButton->setVisible(sel);

  this->ui->apocalipseButton->setVisible(!sel);
  this->ui->actionSpecialButton->setVisible(!sel);
  this->ui->entrainerButton->setVisible(!sel);
}
