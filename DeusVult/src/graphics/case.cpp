#include "case.h"
#include <QtWidgets>

Case::Case(const QColor &color, int x, int y)
{

    this->x = x;
    this->y = y;
    this->color = color;
    setZValue((x + y) % 2);

    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);
}

QRectF Case::boundingRect()const
{
    return QRectF(0, 0, 110, 70);
}

QPainterPath Case::shape() const
{
    QPainterPath path;
    path.addRect(14, 14, 82, 42);
    return path;
}
void Case::paint(QPainter* painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QImage image("/home/desmondes/Documents/POAENSIBS/Graphics/Case.png");

   painter->drawImage(option->rect,image);
}
void Case::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

}

void Case::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

}

void Case::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

}
