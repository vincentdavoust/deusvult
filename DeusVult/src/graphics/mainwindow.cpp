#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QRect rect = this->geometry();

    rect.setX(1);
    rect.setY(1);

    ui->frame->setGeometry(rect);
    ui->frame_2->setGeometry(rect);
    ui->frame_3->setGeometry(rect);


    ui->frame_2->hide();
    ui->frame_3->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_4_clicked()
{
    std::cout << ui->lineEdit->text().toStdString()<< std::endl;
}

void MainWindow::on_pushButton_clicked()
{
    ui->frame->hide();
    ui->frame_3->show();

}

void MainWindow::on_pushButton_3_clicked()
{
    ui->frame->hide();
    ui->frame_2->show();

}

void MainWindow::on_ok_clicked()
{

    std::cout << ui->lineEdit_3->text().toStdString()<< std::endl;

}



void MainWindow::on_pushButton_5_clicked()
{
    ui->frame_2->hide();
    ui->frame->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->frame_3->hide();
    ui->frame->show();
}

void MainWindow::on_frame_3_objectNameChanged(const QString &objectName)
{

}
