/*
** Joueur.cpp for Joueur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sun Nov 13 13:10:35 2016 Vincent Davoust
** Last update Mon Nov 21 11:52:40 2016 Vincent Davoust
*/

#include "joueur.h"

Joueur::Joueur(int numero,
	       Representant *dieu,
	       int argent,
	       int troupes) {
  this->numero = numero;
  this->dieu = dieu;
  this->argent = argent;
  this->troupes = troupes;
}


Representant* Joueur::getRepresentant() {
  return this->dieu;
}

int Joueur::getArgent() {
  return this->argent;
}

int Joueur::getTroupes() {
  return this->troupes;
}

int Joueur::getNumero() {
  return this->numero;
}

int Joueur::depenserArgent(int i) {
  this->argent -= i;
  return this->getArgent();
}
int Joueur::recolterArgent(int i) {
  this->argent += i;
  return this->getArgent();
}

int Joueur::entrainerTroupes(int i) {
  this->troupes += i;
  return this->getTroupes();
}

int Joueur::perdreTroupes(int i) {
  this->troupes -= i;
  return this->getTroupes();
}
