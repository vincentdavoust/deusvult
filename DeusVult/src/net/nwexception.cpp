/*
** nwexception.cpp for nwexception in /home/vincent/Documents/ensibs/2a/pooa/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Mon Nov 21 12:04:43 2016 Vincent Davoust
** Last update Mon Nov 21 12:09:49 2016 Vincent Davoust
*/

#include "nwexception.h"

NWException::NWException(char* e) {
  memcpy(this->msg, e, BUFFER_LENGTH);
}

char* NWException::toString() {
  return this->msg;
}
