/*
** Serveur.cpp for Serveur in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:02 2016 Vincent Davoust
** Last update Mon Nov 21 12:11:58 2016 Vincent Davoust
*/

#include "serveur.h"


Serveur::Serveur(int port) {
  Reseau();
  for (int i=0; i<MAX_PLAYERS-1;i++) {
    clientSocketfd[i] = -1;
  }
  struct sockaddr_in serv_addr, cli_addr;
  socklen_t cli_len;

  // bind to port
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);
  if (bind(socketfd, (struct sockaddr *) &serv_addr,
	   sizeof(serv_addr)) < 0)
    throw new NWException("ERROR on binding");

  // listen
  listen(socketfd, 10);

  // new transmission socket (in socketfd) and backup old socket in serverSocketfd
  serverSocketfd = socketfd;
  clientSocketfd[0] = accept(serverSocketfd, (struct sockaddr*) &cli_addr, &cli_len);
  if (clientSocketfd[0] < 0) {
    throw new NWException("Error to accept connection");
  }
}

Plateau Serveur::getTour(int joueur) {
  // configure from which client to get turn
  socketfd = clientSocketfd[joueur];

  // Reseau::getEtat
  return Reseau::getEtat();
}

void Serveur::sendEtat(Plateau plateau) {
  for (int i=0; i < MAX_PLAYERS-1; i++) {
    if (clientSocketfd < 0)
      continue;
    socketfd = clientSocketfd[i];
#ifdef DEBUG
    std::cout << "Sending state to client " << socketfd << i <<"\n";
#endif
    Reseau::sendEtat(plateau);
  }
}

Serveur::~Serveur() {
#ifdef DEBUG
  std::cout << "\n\ndeleting mothafoka tabernak\n\n";
#endif
  close(serverSocketfd);
  for (int i=0; i<MAX_PLAYERS-1;i++) {
    if (clientSocketfd[i] >= 0)
      close(clientSocketfd[i]);
  }
}
