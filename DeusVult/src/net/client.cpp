/*
** Client.cpp for Client in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:03 2016 Vincent Davoust
** Last update Mon Nov 21 12:14:06 2016 Vincent Davoust
*/

#include "client.h"


Client::Client (char* ip, int port) {
  Reseau();
  // connect to server
  struct sockaddr_in serv_addr;

  // find server
  struct hostent *server = gethostbyname(ip);
  if (server == NULL) {
    throw new NWException("No such server");
  }

  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,
	(char *)&serv_addr.sin_addr.s_addr,
	server->h_length);
  serv_addr.sin_port = htons(port);
  if (connect(socketfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    throw new NWException("ERROR connecting");

}
