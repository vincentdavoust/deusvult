/*
** Reseau.cpp for Reseau in /home/vincent/Documents/ensibs/2a/POOA/cpp/DeusVult/DeusVult
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Sat Nov 12 19:27:02 2016 Vincent Davoust
** Last update Mon Nov 21 12:13:11 2016 Vincent Davoust
*/

#include "reseau.h"

Reseau::Reseau() {
  socketfd = socket(AF_INET, SOCK_STREAM, 0);
  if (socketfd < 0) {
    throw NWException("ERROR openning socket");
  }

}

Reseau::~Reseau() {
  close(socketfd);
}


void Reseau::sendEtat(Plateau plateau) {
  char buffer[BUFFER_LENGTH];
  bzero(buffer, BUFFER_LENGTH);

  // convert Plateau to byte[]
  memcpy(buffer, plateau.toBytes(), (BUFFER_LENGTH > strlen(plateau.toBytes())? strlen(plateau.toBytes()) : BUFFER_LENGTH));

  // write to socket
  int check =  write(socketfd, buffer, BUFFER_LENGTH);
  if (check < 0) {
    throw new NWException("ERROR writing to socket");
  }
}


Plateau Reseau::getEtat() {
  char buffer[BUFFER_LENGTH];
  // read from socket
  int check = read(socketfd, buffer, BUFFER_LENGTH);
  if (check < 0) {
    throw new NWException("ERROR reading from socket");
  }
  // convert buffer to Plateau
  Plateau *p = new Plateau(buffer);

  return *p;
}
