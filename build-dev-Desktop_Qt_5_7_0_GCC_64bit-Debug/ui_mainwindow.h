/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *frame;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QFrame *frame_2;
    QPushButton *ok;
    QLineEdit *lineEdit_3;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *pushButton_5;
    QSpinBox *port_2;
    QLabel *Name_2;
    QLineEdit *lineEdit_2;
    QLabel *Name_4;
    QComboBox *comboBox_2;
    QFrame *frame_3;
    QPushButton *pushButton_4;
    QLabel *label;
    QPushButton *pushButton_2;
    QLabel *label_2;
    QLabel *wait_co;
    QLabel *label_5;
    QSpinBox *joueurs;
    QSpinBox *taille;
    QSpinBox *port;
    QLabel *Name;
    QLineEdit *lineEdit;
    QComboBox *comboBox;
    QLabel *Name_3;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(650, 437);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(0, 0, 521, 321));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        pushButton = new QPushButton(frame);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(130, 60, 80, 22));
        pushButton_3 = new QPushButton(frame);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(130, 120, 80, 22));
        pushButton->raise();
        pushButton_3->raise();
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(180, 140, 391, 231));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        ok = new QPushButton(frame_2);
        ok->setObjectName(QStringLiteral("ok"));
        ok->setGeometry(QRect(260, 200, 80, 22));
        lineEdit_3 = new QLineEdit(frame_2);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(190, 60, 113, 22));
        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(120, 60, 59, 14));
        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(120, 100, 59, 14));
        pushButton_5 = new QPushButton(frame_2);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(40, 200, 80, 22));
        port_2 = new QSpinBox(frame_2);
        port_2->setObjectName(QStringLiteral("port_2"));
        port_2->setGeometry(QRect(191, 95, 80, 23));
        port_2->setMinimum(1);
        port_2->setMaximum(65353);
        port_2->setValue(4444);
        Name_2 = new QLabel(frame_2);
        Name_2->setObjectName(QStringLiteral("Name_2"));
        Name_2->setGeometry(QRect(80, 130, 59, 14));
        lineEdit_2 = new QLineEdit(frame_2);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(170, 120, 113, 22));
        Name_4 = new QLabel(frame_2);
        Name_4->setObjectName(QStringLiteral("Name_4"));
        Name_4->setGeometry(QRect(80, 160, 59, 14));
        comboBox_2 = new QComboBox(frame_2);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(160, 160, 101, 22));
        frame_3 = new QFrame(centralWidget);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setGeometry(QRect(270, 0, 371, 241));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        pushButton_4 = new QPushButton(frame_3);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(270, 200, 80, 22));
        label = new QLabel(frame_3);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 31, 121, 20));
        pushButton_2 = new QPushButton(frame_3);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(20, 200, 80, 22));
        label_2 = new QLabel(frame_3);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 110, 59, 14));
        wait_co = new QLabel(frame_3);
        wait_co->setObjectName(QStringLiteral("wait_co"));
        wait_co->setGeometry(QRect(70, 150, 241, 20));
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(40, 72, 121, 20));
        joueurs = new QSpinBox(frame_3);
        joueurs->setObjectName(QStringLiteral("joueurs"));
        joueurs->setGeometry(QRect(170, 31, 81, 24));
        joueurs->setMinimum(1);
        joueurs->setMaximum(4);
        joueurs->setValue(2);
        taille = new QSpinBox(frame_3);
        taille->setObjectName(QStringLiteral("taille"));
        taille->setGeometry(QRect(171, 69, 81, 24));
        taille->setMinimum(5);
        taille->setMaximum(25);
        taille->setValue(10);
        port = new QSpinBox(frame_3);
        port->setObjectName(QStringLiteral("port"));
        port->setGeometry(QRect(171, 106, 81, 24));
        port->setMinimum(1);
        port->setMaximum(65535);
        port->setValue(4444);
        Name = new QLabel(frame_3);
        Name->setObjectName(QStringLiteral("Name"));
        Name->setGeometry(QRect(60, 140, 59, 14));
        lineEdit = new QLineEdit(frame_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(160, 140, 113, 21));
        comboBox = new QComboBox(frame_3);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(160, 170, 101, 22));
        Name_3 = new QLabel(frame_3);
        Name_3->setObjectName(QStringLiteral("Name_3"));
        Name_3->setGeometry(QRect(60, 170, 59, 14));
        MainWindow->setCentralWidget(centralWidget);
        frame->raise();
        frame_3->raise();
        frame_2->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 650, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DeusVult - The Game", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Serveur", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Client", 0));
        ok->setText(QApplication::translate("MainWindow", "ok", 0));
        lineEdit_3->setText(QApplication::translate("MainWindow", "127.0.0.1", 0));
        label_3->setText(QApplication::translate("MainWindow", "IP", 0));
        label_4->setText(QApplication::translate("MainWindow", "Port", 0));
        pushButton_5->setText(QApplication::translate("MainWindow", "retour", 0));
        Name_2->setText(QApplication::translate("MainWindow", "Name", 0));
        lineEdit_2->setText(QApplication::translate("MainWindow", "client", 0));
        Name_4->setText(QApplication::translate("MainWindow", "Type", 0));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Type Guerier", 0)
         << QApplication::translate("MainWindow", "Type Monaitaire", 0)
         << QApplication::translate("MainWindow", "Type Parasite", 0)
         << QApplication::translate("MainWindow", "Type Sprituel", 0)
        );
        pushButton_4->setText(QApplication::translate("MainWindow", "Cree", 0));
        label->setText(QApplication::translate("MainWindow", "Nombre de joueurs", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "retour", 0));
        label_2->setText(QApplication::translate("MainWindow", "Port", 0));
        wait_co->setText(QApplication::translate("MainWindow", "Waiting for clients to connect...", 0));
        label_5->setText(QApplication::translate("MainWindow", "Taille du plateau", 0));
        Name->setText(QApplication::translate("MainWindow", "Name", 0));
        lineEdit->setText(QApplication::translate("MainWindow", "serveur", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Type Guerier", 0)
         << QApplication::translate("MainWindow", "Type Monaitaire", 0)
         << QApplication::translate("MainWindow", "Type Parasite", 0)
         << QApplication::translate("MainWindow", "Type Sprituel", 0)
        );
        Name_3->setText(QApplication::translate("MainWindow", "Type", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
