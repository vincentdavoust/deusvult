/********************************************************************************
** Form generated from reading UI file 'barregestion.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BARREGESTION_H
#define UI_BARREGESTION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_barregestion
{
public:
    QFrame *Joueur;
    QProgressBar *progressBar;
    QLabel *label;
    QLCDNumber *nbRessources;
    QLabel *label_2;
    QLabel *label_3;
    QPushButton *apocalipseButton;
    QPushButton *apparitionButton;
    QPushButton *messieButton;
    QLCDNumber *nbCroyants;
    QPushButton *construireButton;
    QPushButton *conquerirButton;
    QLCDNumber *nbTroupes;
    QLabel *label_4;
    QPushButton *entrainerButton;
    QPushButton *giveup;
    QFrame *Representant;
    QPushButton *actionSpecialButton;
    QGraphicsView *imageRepresentant;
    QLabel *nomRepresentant;
    QFrame *grisage;

    void setupUi(QWidget *barregestion)
    {
        if (barregestion->objectName().isEmpty())
            barregestion->setObjectName(QStringLiteral("barregestion"));
        barregestion->resize(613, 187);
        barregestion->setStyleSheet(QStringLiteral("background-color: rgb(85, 85, 0);"));
        Joueur = new QFrame(barregestion);
        Joueur->setObjectName(QStringLiteral("Joueur"));
        Joueur->setGeometry(QRect(10, 11, 431, 171));
        Joueur->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 191);"));
        Joueur->setFrameShape(QFrame::StyledPanel);
        Joueur->setFrameShadow(QFrame::Raised);
        progressBar = new QProgressBar(Joueur);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setEnabled(true);
        progressBar->setGeometry(QRect(160, 10, 261, 23));
        progressBar->setValue(24);
        label = new QLabel(Joueur);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 10, 131, 21));
        nbRessources = new QLCDNumber(Joueur);
        nbRessources->setObjectName(QStringLiteral("nbRessources"));
        nbRessources->setGeometry(QRect(150, 80, 41, 23));
        nbRessources->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        nbRessources->setSegmentStyle(QLCDNumber::Flat);
        label_2 = new QLabel(Joueur);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 50, 131, 20));
        label_3 = new QLabel(Joueur);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(70, 80, 81, 21));
        apocalipseButton = new QPushButton(Joueur);
        apocalipseButton->setObjectName(QStringLiteral("apocalipseButton"));
        apocalipseButton->setGeometry(QRect(340, 130, 87, 27));
        apparitionButton = new QPushButton(Joueur);
        apparitionButton->setObjectName(QStringLiteral("apparitionButton"));
        apparitionButton->setGeometry(QRect(340, 70, 87, 27));
        messieButton = new QPushButton(Joueur);
        messieButton->setObjectName(QStringLiteral("messieButton"));
        messieButton->setGeometry(QRect(340, 100, 87, 27));
        nbCroyants = new QLCDNumber(Joueur);
        nbCroyants->setObjectName(QStringLiteral("nbCroyants"));
        nbCroyants->setGeometry(QRect(150, 50, 41, 21));
        QFont font;
        font.setFamily(QStringLiteral("NanumBarunGothic"));
        font.setPointSize(9);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        nbCroyants->setFont(font);
        nbCroyants->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"color: rgb(4, 4, 4);\n"
"font: 75 9pt \"NanumBarunGothic\";"));
        nbCroyants->setFrameShape(QFrame::Box);
        nbCroyants->setFrameShadow(QFrame::Raised);
        nbCroyants->setLineWidth(1);
        nbCroyants->setSegmentStyle(QLCDNumber::Flat);
        construireButton = new QPushButton(Joueur);
        construireButton->setObjectName(QStringLiteral("construireButton"));
        construireButton->setGeometry(QRect(240, 100, 87, 27));
        conquerirButton = new QPushButton(Joueur);
        conquerirButton->setObjectName(QStringLiteral("conquerirButton"));
        conquerirButton->setGeometry(QRect(240, 70, 87, 27));
        nbTroupes = new QLCDNumber(Joueur);
        nbTroupes->setObjectName(QStringLiteral("nbTroupes"));
        nbTroupes->setGeometry(QRect(150, 110, 41, 21));
        nbTroupes->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        nbTroupes->setSegmentStyle(QLCDNumber::Flat);
        label_4 = new QLabel(Joueur);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(90, 110, 61, 20));
        entrainerButton = new QPushButton(Joueur);
        entrainerButton->setObjectName(QStringLiteral("entrainerButton"));
        entrainerButton->setGeometry(QRect(240, 130, 87, 27));
        giveup = new QPushButton(Joueur);
        giveup->setObjectName(QStringLiteral("giveup"));
        giveup->setGeometry(QRect(3, 140, 87, 27));
        label->raise();
        progressBar->raise();
        nbRessources->raise();
        label_2->raise();
        label_3->raise();
        apocalipseButton->raise();
        apparitionButton->raise();
        messieButton->raise();
        nbCroyants->raise();
        construireButton->raise();
        conquerirButton->raise();
        nbTroupes->raise();
        label_4->raise();
        entrainerButton->raise();
        giveup->raise();
        Representant = new QFrame(barregestion);
        Representant->setObjectName(QStringLiteral("Representant"));
        Representant->setGeometry(QRect(450, 10, 151, 171));
        Representant->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 191);"));
        Representant->setFrameShape(QFrame::StyledPanel);
        Representant->setFrameShadow(QFrame::Raised);
        actionSpecialButton = new QPushButton(Representant);
        actionSpecialButton->setObjectName(QStringLiteral("actionSpecialButton"));
        actionSpecialButton->setGeometry(QRect(10, 130, 121, 31));
        imageRepresentant = new QGraphicsView(Representant);
        imageRepresentant->setObjectName(QStringLiteral("imageRepresentant"));
        imageRepresentant->setGeometry(QRect(20, 30, 101, 81));
        nomRepresentant = new QLabel(Representant);
        nomRepresentant->setObjectName(QStringLiteral("nomRepresentant"));
        nomRepresentant->setGeometry(QRect(20, 10, 111, 16));
        grisage = new QFrame(barregestion);
        grisage->setObjectName(QStringLiteral("grisage"));
        grisage->setGeometry(QRect(9, 10, 591, 171));
        grisage->setStyleSheet(QStringLiteral("background-color: rgba(186, 186, 186, 120);"));
        grisage->setFrameShape(QFrame::StyledPanel);
        grisage->setFrameShadow(QFrame::Raised);

        retranslateUi(barregestion);

        QMetaObject::connectSlotsByName(barregestion);
    } // setupUi

    void retranslateUi(QWidget *barregestion)
    {
        barregestion->setWindowTitle(QApplication::translate("barregestion", "barregestion", 0));
        label->setText(QApplication::translate("barregestion", "Influence mondiale :", 0));
        label_2->setText(QApplication::translate("barregestion", "Nombre de croyants", 0));
        label_3->setText(QApplication::translate("barregestion", " Ressources", 0));
        apocalipseButton->setText(QApplication::translate("barregestion", "Apocalipse", 0));
        apparitionButton->setText(QApplication::translate("barregestion", "Apparition", 0));
        messieButton->setText(QApplication::translate("barregestion", "Messie", 0));
        construireButton->setText(QApplication::translate("barregestion", "Construire", 0));
        conquerirButton->setText(QApplication::translate("barregestion", "Conquerir", 0));
        label_4->setText(QApplication::translate("barregestion", " Troupes", 0));
        entrainerButton->setText(QApplication::translate("barregestion", "Entrainer", 0));
        giveup->setText(QApplication::translate("barregestion", "Abandon", 0));
        actionSpecialButton->setText(QApplication::translate("barregestion", "Action Sp\303\251ciale", 0));
        nomRepresentant->setText(QApplication::translate("barregestion", "NomRepresentant", 0));
    } // retranslateUi

};

namespace Ui {
    class barregestion: public Ui_barregestion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BARREGESTION_H
