/********************************************************************************
** Form generated from reading UI file 'frame.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRAME_H
#define UI_FRAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlateauWindow
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;

    void setupUi(QFrame *PlateauWindow)
    {
        if (PlateauWindow->objectName().isEmpty())
            PlateauWindow->setObjectName(QStringLiteral("PlateauWindow"));
        PlateauWindow->resize(633, 385);
        PlateauWindow->setFrameShape(QFrame::StyledPanel);
        PlateauWindow->setFrameShadow(QFrame::Raised);
        gridLayoutWidget = new QWidget(PlateauWindow);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 631, 381));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);

        retranslateUi(PlateauWindow);

        QMetaObject::connectSlotsByName(PlateauWindow);
    } // setupUi

    void retranslateUi(QFrame *PlateauWindow)
    {
        PlateauWindow->setWindowTitle(QApplication::translate("PlateauWindow", "Frame", 0));
    } // retranslateUi

};

namespace Ui {
    class PlateauWindow: public Ui_PlateauWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRAME_H
